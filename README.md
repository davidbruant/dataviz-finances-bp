# Dataviz finances Bordeaux Métropole

Ce projet contient le code qui permet de générer les dataviz d'explorations des BP et CA de Bordeaux (et bientôt d'autres communes de Bordeaux Métropole)


## Pré-requis 

Node.js : v20.10 ou supérieur
npm : 5 ou supérieur


### Installation / Paramétrage de l'outil

#### Installation node pour WINDOWS

- Téléchargement de l'install node pour la version 20.10 :
https://nodejs.org/dist/v20.10.0/node-v20.10.0-x86.msi


#### Installation node pour MacOS / Linux

- Installation de nvm : https://github.com/nvm-sh/nvm#installing-and-updating
- Installation spécifique de Node en v20.10 (npx : permet le téléchargement et l'execution dans la foulée d'un package)
  - Se déplacer à la racine du projet dataviz-finances-bp : cd [mon dossier]`\GitLab\dataviz_finances\dataviz-finances-bp`
  - Lancer l'installation : `nvm install` (nvm s'appuie  la version de node spécifiée dans le fichier .nvmrc à la racine du projet dataviz-finances-bp)
  - Utiliser cette version de node : `nvm use`
  - Test : `node -v` --> doit être à la version `v20.10`


#### Installation dépendances du projet (commun Windows/MacOS/Linux)

Cloner le projet avec git (`git clone`) ou récupérer les sources depuis gitlab et aller dans le dossier du projet.

Ensuite, installer de toutes les dépendances dont le projet à besoin (listées dans le fichier `package.json`, dans `dependencies`/`devDependencies`) : `npm install`


## Créer les données textuelles et financières

### Les données source

#### Données financières

Mettre les fichiers que l'on souhaite visualiser dans les dossiers `données/<commune>/BP` ou `données/<commune>/CA` selon ce qui est souhaité. Bien ranger les fichiers, parce que l'outil ne va pas vérifier si les fichiers dans le dossier BP sont bien des BP, etc.


#### Données textuelles ("labels")

Aller dans le répertoire contenant les sources de l’application React. Dans notre cas, ce serait `c:\react\dataviz-finances-bp`.

Ouvrir/importer dans Excel et éditer le fichier `données/commun/textes/commun.csv` (ou `bp.csv` ou `ca.csv`) et faire toutes les modifications désirées.


#### Sauvegarde en csv

Une fois ce travail terminé, suivre la séquence suivante :
- Fichier => Sauvegarder sous
- Choisir le format "CSV UTF-8 (délimité par des point-virgules)"
- Nommer le fichier `commun.csv` (ou `bp.csv` ou `ca.csv` selon ce qui avait été modifié) et remplacer le fichier existant au même endroit que celui que l'on a récupéré (dossier `données/commun/textes`)


### Créer les fichiers pour le site web

Pour créer toutes les données nécessaires, lancer `npm run build:data`. Cette commande sert à la fois au build du CI et au build en production (le build du CI est sensé créer des conditions similaires à la production)

`npm run build:data` :
1) retrouve les bons plans de compte
2) créé les fichiers `finance-(BP|CA).json` qui reprend les données financières dans les CA/BP agrégées selon l'agrégation décrite dans `données/commun/description-agrégation.json`
3) créé le fichier `doc-budg-strings.json` qui reprend les noms des fonctions et natures trouvées dans les plans de compte
4) créé les fichiers avec les labels


## Pour tester en local

Pour le build rollup et sass : `npm run dev`\
Pour lancer un serveur http local : `npm start`

L'application est disponible sur [http://localhost:8080](http://localhost:8080)
