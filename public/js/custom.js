// JavaScript Document

/// COLLAPSE INFO BULLE AFFICHE SI COUPE ////

$(document).on('mouseover', '.title-collapse', function () {
	var needEllipsis = $(this).css('text-overflow') && (this.offsetHeight < this.scrollHeight);
	var hasNotTitleAttr = typeof $(this).attr('data-title') === 'undefined';
	var titleContent = $(this).attr('data-title');

	if (needEllipsis === true) {

		$(this).parent().append("<div class='infos-bulle'>" + titleContent + "</div>");
	}

	if (needEllipsis === false && hasNotTitleAttr == false) {

		$(this).siblings('div.infos-bulle').remove();
	}
});

$(document).on('mouseout', '.title-collapse', function () {

	$(this).siblings('div.infos-bulle ').remove();

});



/// NAVBAR SLIM SCROLL /// 

$(document).scroll(function () {
	if ($(window).scrollTop() >= 120) {
		x
		$('nav').addClass('navbar-slim');

	}
	else {
		$('nav').removeClass('navbar-slim');


	} if ($(window).scrollTop() >= 130) {
		$('.sticky_filter').addClass('sticky-scroll');
		$('h1.fixed-title').addClass('sticky-title');
	} else {
		$('.sticky_filter').removeClass('sticky-scroll');
		$('h1.fixed-title').removeClass('sticky-title');
	}
});





//// ACCORDION //// 


$('#sub-accordion, #sub-accordion2, #sub-accordionTwo').on('shown.bs.collapse', function () {
	var dataParent = $(this).attr("data-parent");
	var hiddenText = $(dataParent).children('.accordion-head');

	$(hiddenText).css("display", "none");
});



$('#sub-accordion, #sub-accordion2,  #sub-accordionTwo').on('hidden.bs.collapse', function () {
	// do something...
	var dataParent = $(this).attr("data-parent");
	var hiddenText = $(dataParent).children('.accordion-head');

	$(hiddenText).css("display", "block");
});





//// CUSTOM SELECT***

var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-selectOne");
l = x.length;
for (i = 0; i < l; i++) {
	selElmnt = x[i].getElementsByTagName("select")[0];
	ll = selElmnt.length;
	/*for each element, create a new DIV that will act as the selected item:*/
	a = document.createElement("DIV");
	a.setAttribute("class", "select-selected");
	a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	x[i].appendChild(a);
	/*for each element, create a new DIV that will contain the option list:*/
	b = document.createElement("DIV");
	b.setAttribute("class", "select-items select-hide");
	for (j = 1; j < ll; j++) {
		/*for each option in the original select element,
		create a new DIV that will act as an option item:*/
		c = document.createElement("DIV");
		c.innerHTML = selElmnt.options[j].innerHTML;
		c.addEventListener("click", function (e) {
			/*when an item is clicked, update the original select box,
			and the selected item:*/
			var y, i, k, s, h, sl, yl;
			s = this.parentNode.parentNode.getElementsByTagName("select")[0];
			sl = s.length;
			h = this.parentNode.previousSibling;
			for (i = 0; i < sl; i++) {
				if (s.options[i].innerHTML == this.innerHTML) {
					s.selectedIndex = i;
					h.innerHTML = this.innerHTML;
					y = this.parentNode.getElementsByClassName("same-as-selected");
					yl = y.length;
					for (k = 0; k < yl; k++) {
						y[k].removeAttribute("class");
					}
					this.setAttribute("class", "same-as-selected");
					break;
				}
			}
			h.click();
		});
		b.appendChild(c);
	}
	x[i].appendChild(b);
	a.addEventListener("click", function (e) {
		/*when the select box is clicked, close any other select boxes,
		and open/close the current select box:*/
		e.stopPropagation();
		closeAllSelect(this);
		this.nextSibling.classList.toggle("select-hide");
		this.classList.toggle("select-arrow-active");
	});
}
function closeAllSelect(elmnt) {
	/*a function that will close all select boxes in the document,
	except the current select box:*/
	var x, y, i, xl, yl, arrNo = [];
	x = document.getElementsByClassName("select-items");
	y = document.getElementsByClassName("select-selected");
	xl = x.length;
	yl = y.length;
	for (i = 0; i < yl; i++) {
		if (elmnt == y[i]) {
			arrNo.push(i)
		} else {
			y[i].classList.remove("select-arrow-active");
		}
	}
	for (i = 0; i < xl; i++) {
		if (arrNo.indexOf(i)) {
			x[i].classList.add("select-hide");
		}
	}
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);



// NAVBAR ///


$('li.nav-item a').on("mouseover", function () {

	$('li.nav-item.active').removeClass('active');
	$(this).parent().toggleClass('active');

});



$(document).on("mouseout", 'li.nav-item', function () {
	$('li.nav-item.active').removeClass('active');
	$('li.nav-item.nav-current').addClass('active');
});



/// BTN DEPENSE RECETTES ///

$('.btn-group .btn-secondary').on("mouseover", function () {
	$('.btn-group .btn-secondary.active').removeClass('active');
	$(this).toggleClass('active');
});


$('.btn-group .btn-secondary').on("mouseout", function () {
	$('.btn-group .btn-secondary.active').removeClass('active');
	$('.btn-group .btn-secondary.current').addClass('active');
});



$('.btn-group .btn-secondary').on("click", function () {
	$('.btn-group .btn-secondary.active').removeClass('active');
	$('.btn-group .btn-secondary.current').removeClass('current');
	$(this).addClass('current');

});





//// ANIMATION DESACTIVATION ////


$(window).scroll(function () {
	var positionScrollY = $(window).scrollTop();
	var heightWindow = parseInt($(window).height() / 1.4);

	var animateFadeIn = $('.animFadeIn');
	var animateFadeInLeft = $('.animFadeInLeft');
	var animateFadeInRight = $('.animFadeInRight');
	var animateSpeedLeft = $('.animSpeedLeft');
	var animateSpeedRight = $('.animSpeedRight');
	var animZoomIn = $('.animZoomIn');
	var animFadeDown = $('.animFadeDown');
	var animBgPrice = $('.animBgPrice');


	$(animateFadeIn).each(function () {

		var positionAnimFadeIn = parseInt($(this).offset().top);
		var positionAnimFadeInBottom = parseInt($(this)[0].getBoundingClientRect().bottom);
		var viewportHeight = $(window).height();
		var pageHeight = $(document).height();


		if ((positionScrollY + heightWindow) >= positionAnimFadeIn || positionScrollY >= (pageHeight - viewportHeight) && positionAnimFadeInBottom >= heightWindow) {
			$(this).removeClass('animate__fadeOut animate__fast');
			$(this).addClass('animate__animated animate__fadeIn animate__fast');

		} else {
			$(this).removeClass('animate__fadeIn');
			$(this).addClass('animate__fadeOut');

		}

	});


	$(animateFadeInLeft).each(function () {

		var positionAnimFadeInLeft = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimFadeInLeft) {
			$(this).removeClass('animate__fadeOutLeft animate__fast');
			$(this).addClass('animate__animated animate__fadeInLeft');

		} else {
			$(this).removeClass('animate__fadeInLeft');
			$(this).addClass('animate__fadeOutLeft');

		}
	});
	$(animateFadeInRight).each(function () {

		var positionAnimFadeInRight = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimFadeInRight) {
			$(this).removeClass('animate__fadeOutRight animate__fast');
			$(this).addClass('animate__animated animate__fadeInRight');

		} else {
			$(this).removeClass('animate__fadeInRight');
			$(this).addClass('animate__fadeOutRight');

		}
	});


	$(animateSpeedLeft).each(function () {

		var positionAnimSpeedLeft = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimSpeedLeft) {
			$(this).removeClass('animate__lightSpeedOutLeft animate__fast');
			$(this).addClass('animate__animated animate__lightSpeedInLeft');

		} else {
			$(this).removeClass('animate__lightSpeedInLeft');
			$(this).addClass('animate__lightSpeedOutLeft');

		}
	});


	$(animateSpeedRight).each(function () {

		var positionAnimSpeedRight = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimSpeedRight) {
			$(this).removeClass('animate__lightSpeedOutRight animate__fast');
			$(this).addClass('animate__animated animate__lightSpeedInRight');

		} else {
			$(this).removeClass('animate__lightSpeedInRight');
			$(this).addClass('animate__lightSpeedOutRight');

		}
	});


	$(animZoomIn).each(function () {

		var positionAnimZoomIn = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimZoomIn) {
			$(this).removeClass('  animate__zoomOut ');
			$(this).addClass('animate__animated animate__zoomIn ');

		} else {
			$(this).removeClass('animate__zoomIn');
			$(this).addClass('animate__zoomOut ');

		}
	});



	$(animFadeDown).each(function () {

		var positionAnimFadeDown = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimFadeDown) {
			$(this).removeClass('animate__fadeOutDown');
			$(this).addClass('animate__animated animate__fadeInDown ');

		} else {
			$(this).removeClass('animate__fadeInDown');
			$(this).addClass('animate__fadeOutDown ');

		}
	});


	$(animBgPrice).each(function () {

		var positionAnimBgPrice = parseInt($(this).offset().top);

		if ((positionScrollY + heightWindow) >= positionAnimBgPrice) {
			$(this).removeClass(' animBgPriceOut ');
			$(this).addClass('animBgPriceIn ');

		} else {
			$(this).removeClass('animBgPriceIn');
			$(this).addClass('animBgPriceOut  ');

		}
	});


});