import { configureStore } from "@reduxjs/toolkit"
import { json, dsv } from 'd3-fetch'

import datavizReducer, {setFinanceData, setFinanceLabels} from "./datavizSlice.js"
import labelsReducer, {setLabels} from "./labelsSlice.js"

export const store = configureStore({
  reducer: {
    dataviz: datavizReducer,
    labels: labelsReducer
  },
})

function getURL(selector){
  const element = document.head.querySelector(selector)

  if(!element){
    throw new TypeError(`Élément ${selector} manquant`)
  }

  const hrefAttribute = element.getAttribute('href')

  if(!hrefAttribute){
    throw new TypeError('Attribut "href" manquant sur link#finance-data')
  }

  return hrefAttribute
}

/**
 * Initialize finance data
 */
const financeDataURL = getURL('link#finance-data')

json(financeDataURL)
.then((/** @type {FinanceData} */ financedata) => {
  console.log('fetch financeData', financedata)
  store.dispatch(setFinanceData(financedata))
})


/**
 * Initialize text data
 */
const textDataURL = getURL('link#text-data')

dsv(';', textDataURL)
.then((/** @type {FinanceData} */ _labels) => {  
  const labels = {}

  for(const {Key, Label} of _labels){
    if(Reflect.getOwnPropertyDescriptor(labels, Key) !== undefined){
      throw new TypeError(`Clef dupliquée. La clef ${Key} apparait plusieurs fois dans le fichier ${textDataURL}`)
    }

    labels[Key] = Label
  }
  
  Object.freeze(labels)

  store.dispatch(setLabels(labels))
})


/**
 * Initialize finance text data
 */
const financeLabelsURL = getURL('link#finance-labels')

json(financeLabelsURL)
.then((/** @type {FinanceData} */ financeLabels) => {
  store.dispatch(setFinanceLabels(financeLabels))
})