//@ts-check

import { createSlice } from "@reduxjs/toolkit";

import '../types.d.js'

import makeAggregationDataTree from "../utils/makeAggregationDataTree.js"
import makeFonctionDataTree from "../utils/makeFonctionDataTree.js"


/** @type {BordeauxMetropoleDatavizState} */
const initialState = {
  financeData: undefined,
  financeLabels: undefined,
  visualisedDataTree: undefined,
  availableYears: undefined,
  selectedYear: undefined,
  selectedType: 'D',
  selectedNode: undefined,
  selectedPresentation: undefined,
  phaseBudgetaire: undefined
}

const DEFAULT_PRESENTATION = 'Agregation'

// caching for perf and because make*DataTree functions don't work if called twice
let cachedAggregationTree;
let cachedFonctionTree;

export const datavizSlice = createSlice({
  name: "dataviz",
  initialState,
  reducers: {
    setFinanceData(state, action){
      const financeData = action.payload
      state.financeData = financeData

      const availableYears = financeData.aggregations.map(aggregationYear => aggregationYear.year);
      availableYears.sort((a, b) => a - b);
      state.availableYears = availableYears
      state.selectedYear = Math.max(...availableYears)

      datavizSlice.caseReducers.selectPresentation(state, {type: '?', payload: state.selectedPresentation || DEFAULT_PRESENTATION})
    },
    setFinanceLabels(state, action){
      state.financeLabels = action.payload

      datavizSlice.caseReducers.selectPresentation(state, {type: '?', payload: state.selectedPresentation || DEFAULT_PRESENTATION})
    },
    selectYear: (state, action) => {
      state.selectedYear = action.payload

      if(state.visualisedDataTree && state.selectedYear && state.selectedType)
        state.selectedNode = state.visualisedDataTree[state.selectedYear][state.selectedType];
    },
    selectType: (state, action) => {
      state.selectedType = action.payload

      if(state.visualisedDataTree && state.selectedYear && state.selectedType)
        state.selectedNode = state.visualisedDataTree[state.selectedYear][state.selectedType];
    },
    /**
     * 
     * @param {BordeauxMetropoleDatavizState} state 
     * @param { {type: string, payload: BordeauxMetropoleDatavizState["selectedPresentation"] } } action 
     */
    selectPresentation: (state, action) => {
      if(!state.financeData || !state.financeLabels){
        return;
      }

      state.selectedPresentation = action.payload
      
      let newVisualisedDataTree;

      switch (state.selectedPresentation) {
        case 'Agregation':
          if(!cachedAggregationTree){
            cachedAggregationTree = makeAggregationDataTree(state.financeData, state.financeLabels);
          }
          newVisualisedDataTree = cachedAggregationTree
          break;
        case 'Fonction':
          if(!cachedFonctionTree){
            cachedFonctionTree = makeFonctionDataTree(state.financeData, state.financeLabels);
          }
          newVisualisedDataTree = cachedFonctionTree
          break;
        case undefined: 
          throw new TypeError('state.selectedPresentation est undefined')
        default:
          /** @type {never} */
          // eslint-disable-next-line
          const _impossible = state.selectedPresentation;
          newVisualisedDataTree = state.visualisedDataTree;
      }

      state.visualisedDataTree = newVisualisedDataTree;

      if(state.visualisedDataTree && state.selectedYear && state.selectedType)
        state.selectedNode = state.visualisedDataTree[state.selectedYear][state.selectedType];
    },
    selectNode: (state, action) => {
      state.selectedNode = action.payload
    },
    setPhaseBudgetaire(state, action){
      state.phaseBudgetaire = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { selectYear, selectType, selectPresentation, selectNode, setPhaseBudgetaire, setFinanceData, setFinanceLabels } = datavizSlice.actions
export default datavizSlice.reducer
