import { createSlice } from "@reduxjs/toolkit"

const initialState = {
  labels: {}
}

export const labelsSlice = createSlice({
  name: "labels",
  initialState,
  reducers: {
    setLabels(state, {payload}){
      state.labels = payload
    }
  },
})
// Action creators are generated for each case reducer function

export default labelsSlice.reducer
export const {setLabels} = labelsSlice.actions