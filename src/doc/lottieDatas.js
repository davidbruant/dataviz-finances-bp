const lottieActions = [
    [
		 {
            visibility: [0.0,0.15],
            type: 'stop', 
			frames: [0],
        },	
        {
            visibility: [0.15,1.00],
            type: 'play', 
        },
		{
            visibility: [0.4, 1.00],
            type: 'stop', 
			frames: [400],
        }	
    ],
    [
		{
            visibility: [0.0, 0.1],
            type: 'stop',
			frames: [0],
        }, 	
        {
            visibility: [0.1, 1.00],
            type: 'play', 
        },
			 {
            visibility: [0.7, 1.00],
            type: 'stop',
			 frames: [400],
        }, 
    ],
    [
		{
            visibility: [0.0, 0.4],
            type: 'stop',
			frames: [0],
        }, 
        {
            visibility: [0.4, 1.00],
            type: 'play', 
        },
        {
            visibility: [0.8, 1.00],
            type: 'stop',
            frames: [400],
        }
    ],
    [
		{
            visibility: [0.0, 0.4],
            type: 'stop',
            frames: [0],
        }, 
			
        {
            visibility: [0.4, 1.00],
            type: 'play', 
        },
        {
            visibility: [0.99, 1.00],
            type: 'stop',
            frames: [300],
        }
    ],
    [
		{
            visibility: [0.0, 0.3],
            type: 'stop',
            frames: [0],
        }, 
			
        {
            visibility: [0.3, 1.00],
            type: 'play', 
        },
        {
            visibility: [0.7, 1.00],
            type: 'stop',
            frames: [600],
        } 
    ],
    [
		{
            visibility: [0.0, 0.3],
            type: 'stop',
            frames: [0],
        }, 
        {
            visibility: [0.3, 1.00],
            type: 'play', 
        },
        {
            visibility: [0.6, 1.00],
            type: 'stop',
            frames: [130],
        }
    ],
    [
		{
            visibility: [0.0, 0.1],
            type: 'stop',
            frames: [0],
        }, 	
        {
            visibility: [0.1, 1.00],
            type: 'play', 
        },
        {
            visibility: [0.6, 1.00],
            type: 'stop',
            frames: [130],
        }
    ],
    [
		{
            visibility: [0.0, 0.4],
            type: 'stop',
            frames: [0],
        }, 
        {
            visibility: [0.4, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.4, 1.00],
            type: 'stop',
            frames: [220],
        }
    ],
    [
		{
            visibility: [0.0, 0.2],
            type: 'stop',
			frames: [0],
        }, 
			
        {
            visibility: [0.2, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.6, 1.00],
            type: 'stop',
			frames: [220],
        }
    ],
    [
		{
            visibility: [0.0, 0.5],
            type: 'stop',
			frames: [0],
        }, 
			
        {
            visibility: [0.5, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.7, 1.00],
            type: 'stop',
			frames: [220],
        }
    ],
    [
		{
            visibility: [0.0, 0.4],
            type: 'stop',
			frames: [0],
        }, 
			
        {
            visibility: [0.4, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.6, 1.00],
            type: 'stop',
			frames: [220],
        }
    ],
    [
		{
            visibility: [0.0, 0.4],
            type: 'stop',
			 frames: [0],
        }, 
			
        {
            visibility: [0.4, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.6, 1.00],
            type: 'stop',
			frames: [220],
        }
    ],
    [
		{
            visibility: [0.0, 0.3],
            type: 'stop',
			frames: [0],
        }, 
			
        {
            visibility: [0.3, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.5, 1.00],
            type: 'stop',
			frames: [120],
        }
    ],
    [
		{
            visibility: [0.0, 0.3],
            type: 'stop',
			 frames: [0],
        }, 
        {
            visibility: [0.3, 1.00],
            type: 'play', 
        },
		{
            visibility: [0.5, 1.00],
            type: 'stop',
			frames: [120],
        }
    ],
    [
        {
            visibility:[0.0, 0.3],
            type: "seek",
            frames: [0, 64]
        },
        {
            visibility:[0.3 , 0.32],
            type: "play",
            frames: [0,64]
        }
    ],
    [
        {
            visibility:[0.1 , 0.5],
            type: "seek",
            frames: [0,85]
        }
    ],
    [
        {
            visibility:[0.1 , 0.5],
            type: "seek",
            frames: [0,85]
        }
    ],
    [
        {
            visibility:[0.1 , 0.5],
            type: "seek",
            frames: [0,85]
        }
    ],
    [
        {
            visibility:[0.1 , 0.5],
            type: "seek",
            frames: [0,85]
        }
    ],
    [
	    {
            visibility:[0.0, 0.1],
            type: "stop",
            frames : [0],   
        }, 
        {
            visibility:[0.3, 1.00],
            type: "play",
        },
		{
            visibility:[0.9, 1.00],
            type: "stop",
		    frames : [250],
        }
    ],
    [
		{
            visibility:[0.0, 0.3],
            type: "stop",
            frames: [0],
        },
        {
            visibility:[0.3, 1.00],
            type: "play",
        },
        {
            visibility:[0.99, 1.00],
            type: "stop",
		    frames: [240],
        },
    ],
    [
	    {
            visibility:[0.0, 0.05],
            type: "stop",
            frames: [0],
        }, 
        {
            visibility:[0.3, 1.00],
            type: "play",
        },
		{
            visibility:[0.6, 1.00],
            type: "stop",
		    frames: [300],
        }
    ]
]

const lottieIds = [
    { id: "#firstLottie" },
    { id: "#secondLottie" },
    { id: "#threeLottie" },
    { id: "#fourLottie" },
    { id: "#balanceLottie" },
    { id: "#fiveLottie" },
    { id: "#bgLottiePrice1" },
    { id: "#sixLottie-mob" },
    { id: "#sixLottie" },
    { id: "#bgLottiePrice2" },
    { id: "#sevenLottie" },
    { id: "#balanceLottieTwo" },
    { id: "#bgLottiePrice3" },
    { id: "#nineLottie" },
    { id: "#eightLottie" },
    { id: "#bgLottiePrice4" },
    { id: "#twelve2Lottie" },
    { id: "#tenLottie" },
    { id: "#elevenLottie" },
    { id: "#twelveLottie" },
    { id: "#balanceLottieThree" },
    { id: "#bgLottieBtn" }
]

export { lottieActions, lottieIds }
