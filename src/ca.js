import React, { StrictMode } from 'react'
import ReactDOM from 'react-dom'
import { store } from "./redux/store.js"
import { Provider } from "react-redux"
import CA from './CA.jsx'

ReactDOM.render(
   <StrictMode>
      <Provider store={store}>
         <CA />
      </Provider>
   </StrictMode>
   , document.getElementById('root')
)
