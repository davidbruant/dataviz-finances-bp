//@ts-check

import './types.d.js'

/**
 * 
 * @param {any} data 
 * @returns {data is AugmentedAggregationNode}
 */
export function isAugmentedAggregationNode(data){
    return Object(data) === data && Array.isArray(data.children)
}