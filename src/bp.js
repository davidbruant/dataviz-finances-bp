import React, { StrictMode } from 'react'
import ReactDOM from 'react-dom'
import { store } from "./redux/store.js"
import { Provider } from "react-redux"
import BP from './BP.jsx'

ReactDOM.render(
   <StrictMode>
      <Provider store={store}>
         <BP />
      </Provider>
   </StrictMode>
   , document.getElementById('root')
)
