
/**
 * @typedef {Object} LigneDocumentBudgetaire
 * @property {string} Nature
 * @property {string} Fonction
 * @property {string} CodRD // 'R' | 'D'
 * @property {number} MtReal
 * @property {number} MtPrev
 * @property {number} Montant
 */

/**
 * @typedef {Object} DocumentBudgetaire
 * @property {string} LibelleColl
 * @property {string} IdColl
 * @property {string} Nomenclature
 * @property {string} NatDec
 * @property {number} Exer
 * @property {LigneDocumentBudgetaire[]} rows
 */

/**
 * @typedef {Object} AggregationNode
 * @property {string} id
 * @property {string} name
 * @property {AggregationTree[]} children
 */

/**
 * @typedef {Object} AggregationLeaf
 * @property {string} id
 * @property {string} name
 * @property {LigneDocumentBudgetaire[]} elements
 */

/**
 * @typedef {AggregationNode | AggregationLeaf} AggregationTree
 */


/**
 * @typedef {Object} Aggregation
 * @property {number} year
 * @property {AggregationTree} aggregation
 */

 
/**
 * @typedef {Object} FinanceData
 * @property {DocumentBudgetaire[]} documentBudgetaires
 * @property {Aggregation[]} aggregations
 */


/**
 * Les types à partir d'ici documentent ce qu'il se passe dans 
 * la fonction buildTree de commons.js
 * Idéalement ces types disparaitront. Ou au moins, on pourra transformer les types
 * issus de finance-data.json en types ReadOnly et supprimer les mutations sur les données
 */

/**
 * @typedef {Object} AggregationNodeAugmentation
 * @property {string} newId
 * @property {string} deepId
 * @property {string} color
 * @property {number} size
 * @property {number} cumulatedSize
 * @property {number} infoSize
 * @property {AugmentedAggregationTree[]} children
 */

/**
 * @typedef {AggregationNode & AggregationNodeAugmentation} AugmentedAggregationNode
 */

/**
 * @typedef {Object} AggregationLeafAugmentation
 * @property {string} newId
 * @property {string} deepId
 * @property {string} color
 * @property {number} size
 * @property {number} cumulatedSize
 * @property {number} infoSize
 */

/**
 * @typedef {AggregationLeaf & AggregationLeafAugmentation} AugmentedAggregationLeaf
 */


/**
 * @typedef {AugmentedAggregationNode | AugmentedAggregationLeaf} AugmentedAggregationTree
 */


/**
 * @typedef {Object} YearlyDataCollected
 * @property { AugmentedAggregationTree } R
 * @property { AugmentedAggregationTree } D
 */

/**
 * @typedef { {[key: number]: YearlyDataCollected} } VisualisedDataTree
 */


/**
 * @typedef { number[] } AvailableYears
 */

/** @typedef {'Agregation' | 'Fonction'} FinanceVizPrésentation */
/** @typedef {'BP' | 'CA'} PhaseBudgetaire */

/**
 * @typedef {Object} BordeauxMetropoleDatavizState
 * @property {FinanceData | undefined} financeData  
 * @property {any} financeLabels  
 * @property {VisualisedDataTree | undefined} visualisedDataTree
 * @property {AvailableYears | undefined} availableYears
 * @property {number | undefined} selectedYear
 * @property {'D' | 'R'} selectedType
 * @property {AugmentedAggregationTree | undefined} selectedNode
 * @property {FinanceVizPrésentation | undefined} selectedPresentation
 * @property {PhaseBudgetaire | undefined} phaseBudgetaire
 */