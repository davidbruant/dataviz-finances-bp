import React from "react"
import PropTypes from "prop-types"

import Select from "react-select"

const SelectPerso = ({ value, isSearchable, options, title, ...props }) => {
  const customStyles = {
    dropdownIndicator: base => ({
      ...base,
      color: "red" // Custom colour
    }),
    indicatorSeparator: base => ({
      ...base,
      display:'none',
    }),
    
    control: base => ({
        ...base,
        borderRadius: 30,
        border: 0,
        // This line disable the blue border
        boxShadow: 'none',
        cursor:'pointer',
      
    }),
    singleValue: base => ({
        ...base,
        fontWeight: 600,
        fontSize: 15,
        marginLeft: 10,
        cursor:'pointer',
    }),
    menu: base => ({
        ...base,
    }),
    menuList: base => ({
        ...base,
    }),
    option: (styles, { isDisabled, isFocused, isSelected }) => {
        const colorBackground = '#c3c4cd'
        const colorFocus = '#F0F2F3'
        return {
        ...styles,
        fontWeight: 600,
        fontSize: 15,
        textAlign: 'center',
        backgroundColor: isDisabled
            ? undefined
            : isSelected
            ? colorBackground
            : isFocused
            ? colorFocus
            : undefined,
        color: isDisabled
            ? colorFocus
            : 'black',
        cursor: isDisabled ? 'not-allowed'
            : isSelected
            ? 'no-drop'
            : 'pointer',
        ':active': {
            ...styles[':active'],
            backgroundColor: !isDisabled
            ? colorBackground
            : undefined,
        },
        }
    },
  }

  const handleInputChange = (e) => {
    props.onChange(e)
  }

  return <Select styles={customStyles} isSearchable={isSearchable} options={options} value={{label:value,value:value}} onChange={handleInputChange}/>
}
  
SelectPerso.propTypes = {
  value: PropTypes.number.isRequired,
  isSearchable: PropTypes.bool.isRequired,
  options: PropTypes.array.isRequired
}

export default SelectPerso