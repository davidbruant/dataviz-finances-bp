import React from 'react'
import { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'

import { useSelector } from 'react-redux'

function Header() {

    const labels = useSelector((state) => state.labels.labels)
    const phaseBudgetaire = useSelector((state) => state.dataviz.phaseBudgetaire)
    const [navClasses, setNavClasses] = useState("navbar navbar-expand-lg navbar-light")

    const stickyOnScroll = () => {
        const scrollCheck = window.scrollY > 130
            if (scrollCheck) {
                setNavClasses("navbar navbar-expand-lg navbar-light navbar-slim")
            }
            else {
                setNavClasses("navbar navbar-expand-lg navbar-light")
            }
    }

    useEffect(() => {
        document.addEventListener("scroll", stickyOnScroll)
        return () => {
            // remove event listener on unload
            document.removeEventListener("scroll", stickyOnScroll)
        }
    }, [])
   
    const location = useLocation()

    return (
        <header>
            <nav className={navClasses}>
                <div className="container">
                    <Link to="/" className="navbar-brand " >
                        <div width="52px" height="62px">
                            <img src="/img/ville-bordeaux.png" alt={ labels['header.logo.description'] } title={ labels['header.logo.description'] }/>
                        </div>
                    </Link>
                    <button className="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                        <span className="navbar-toggler-icon"/>
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="navbar-collapse align-self-end collapse" id="navbarTogglerDemo02">
                        <ul className="mr-auto mt-2 mt-lg-0 navbar-nav pl-2 pl-lg-0 pl-sm-0 pr-2 pr-lg-0 pr-sm-0">
                            <li className={ location.pathname === "/" ? "nav-item nav-current active" : "nav-item " }>
                                <Link to="/" className="nav-link" data-bolder="{ labels['header.lien1.texte'] }">{ labels['header.lien1.texte'] }</Link>
                            </li>
                            <li className={ location.pathname === "/fonction" ? "nav-item nav-current active" : "nav-item " }>
                                <Link to="/fonction" className="nav-link" data-bolder="{ labels['header.lien2.texte'] }">{ labels['header.lien2.texte'] }</Link>
                            </li>
                            {
                                phaseBudgetaire === 'CA' ?
                                (
                                    <li className={ location.pathname === "/compte-administratif" ? "nav-item nav-current active" : "nav-item " }>
                                        <Link to="/compte-administratif" className="nav-link" data-bolder="{ labels['header.lien4.texte'] }">{ labels['header.lien4.texte'] }</Link>
                                    </li>
                                ) : // phaseBudgetaire === 'BP'
                                (
                                    <li className={ location.pathname === "/budget" ? "nav-item nav-current active" : "nav-item " }>
                                        <Link to="/budget" className="nav-link" data-bolder="{ labels['header.lien3.texte'] }">{ labels['header.lien3.texte'] }</Link>
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                    <div className="nav-box">
                        <p><a href={ labels['header.lien.donnees.brutes'] } rel="noreferrer" target="_blank" >{ labels['header.label.donnees.brutes'] }</a></p>
                        <div className="img-nav-box">
                            <a href={ labels['header.lien.open.data'] } rel="noreferrer" target="_blank" >
                                <img src="/img/logo-open-data.png" alt="Nouvelle fenêtre navigateur" title="Nouvelle fenêtre navigateur" />
                            </a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
    )
}

export default Header