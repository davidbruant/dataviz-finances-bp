import React from 'react'
import { useState, useEffect } from 'react'

import { useSelector, useDispatch } from 'react-redux'
import { selectYear, selectType } from '../redux/datavizSlice'

import SelectPerso from '../components/SelectPerso/SelectPerso'

function StickyBar() {

    const availableYears = useSelector((state) => state.dataviz.availableYears)
    const selectedYear = useSelector((state) => state.dataviz.selectedYear)
    const selectedType = useSelector((state) => state.dataviz.selectedType)
    const labels = useSelector((state) => state.labels.labels)

    const [stickyFilterClasses, setStickyFilterClasses] = useState('sticky_filter')
    const [stickyH1Classes, setStickyH1Classes] = useState('hidden-title')
   
    const dispatch = useDispatch()

    const setType = (evt) => {
        evt.preventDefault()
        if (evt) {
            dispatch(selectType(evt.target.value))
        }
    }

    const handleInputChange = (evt) => {
       dispatch(selectYear(evt.value))
    }

    const stickyOnScroll = () => {
        const scrollCheck = window.scrollY > 130
        if (scrollCheck) {
            setStickyFilterClasses("sticky_filter sticky-scroll")
            setStickyH1Classes("sticky-title")
        }
        else {
            setStickyFilterClasses("sticky_filter")
            setStickyH1Classes("hidden-title")
        }
    }

    useEffect(() => {
        document.addEventListener("scroll", stickyOnScroll)
        return () => {
            // remove event listener on unload
            document.removeEventListener("scroll", stickyOnScroll)
        }
    }, [])

    return (
        <div className={stickyFilterClasses}>
            <div className="align-items-center row sticky-row">
                <h1 className={stickyH1Classes}>{ labels['explorer.comptes.titre'] }</h1>

                <div className="col-1 col-lg-4 col-md col-sm-2 col-xl pl-0 pl-lg-3 pl-md-0 pr-1 pr-sm-3 sticky-hidden">
                    <hr />
                </div>
                <div className="col-10 col-8 col-lg-4 col-md-5 col-sm-8 col-xl-3 p-md-0 pl-0 pr-0 sticky-box-options">
                    <div className="box-options">
                        <div className="custom-select-one">
                            {
                                availableYears && selectYear ?
                                    (
                                        <SelectPerso isSearchable={false} options={availableYears.map(y => ({value: y, label: y.toString()}))} value={selectedYear} onChange={handleInputChange} />
                                    )
                                    : 'en chargement'
                            }                            
                        </div>
                        <div className="btn-group btn-group-toggle" >
                        <label className="btn btn-secondary " style={{background:selectedType === "D" ? '#D40A36' : null, color:selectedType === "D" ? 'white' : null}}>
                            <input type="radio" name="options" id="option1" autoComplete="off" value="D" onClick={evt=>setType(evt)} />{ labels['explorer.compte.depenses'] }
                        </label>
                        <label className="btn btn-secondary current" style={{background:selectedType === "R" ?'#D40A36' : null, color:selectedType === "R" ? 'white' : null}}>
                            <input type="radio" name="options" id="option2" autoComplete="off" value="R" onClick={evt=>setType(evt) } />{ labels['explorer.compte.recettes'] }
                        </label>
                    </div>
                    </div>
                </div>
                <div className="col-1 col-lg-4 col-md col-md-3 col-sm-2 col-xl pl-1 pl-sm-3 pr-0 pr-lg-3 pr-md-0 sticky-hidden">
                    <hr />
                </div>
            </div>
        </div>
    )
}

export default StickyBar