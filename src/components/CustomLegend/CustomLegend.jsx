import React from 'react'

import { useSelector, useDispatch } from 'react-redux'

import { selectNode } from '../../redux/datavizSlice'

import { findInTreeByDeepId } from '../../utils/common'

export default function CustomLegend({data}) {

    const visualisedDataTree = useSelector((state) => state.dataviz.visualisedDataTree)
    const selectedYear = useSelector((state) => state.dataviz.selectedYear)
    const selectedType = useSelector((state) => state.dataviz.selectedType)

    const dispatch = useDispatch()

    const handleClickLegend = (e) => {
        e.preventDefault();
        const searchResult = findInTreeByDeepId(e.currentTarget.getAttribute('data-deep-id'), visualisedDataTree[selectedYear][selectedType])
        if ( searchResult !== null && searchResult !== undefined
            && searchResult.hasOwnProperty('children') && searchResult.children.length > 0
            && (!searchResult.hasOwnProperty('elements') || (searchResult.hasOwnProperty('elements') && searchResult.elements.length === 0)) ){
            dispatch(selectNode(searchResult))
        }
    }

    function Label(name, deepId, color, key){
        return (
            <div 
                key={key} 
                data-deep-id={deepId} 
                onClick={handleClickLegend} 
                className='legend-label-container'>
                    <span className='custom-legend-label-first-circle' style={{ backgroundColor: color }}></span>
                    <span className='custom-legend-label-second-circle' style={{ backgroundColor: color }}></span>
                    <p className='custom-legend-label'>{name}</p>
            </div>
        )
    }

    return (
        <div>
            {
                data.map((item, i) => {
                    return Label(item.name, item.deepId, item.symbol.fill, i)
                })
            }
        </div>
    )
}

