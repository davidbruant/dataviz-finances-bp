import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectNode as selectNodeAction } from '../redux/datavizSlice'

import findAncestry from '../utils/findAncestry'

import FilArianeSiblingGroup from './FilArianeSiblingGroup'


function FilAriane() {
   
    const visualisedDataTree = useSelector((state) => state.dataviz.visualisedDataTree)
    const selectedYear = useSelector((state) => state.dataviz.selectedYear)
    const selectedType = useSelector((state) => state.dataviz.selectedType)
    const selectedNode = useSelector((state) => state.dataviz.selectedNode)
    const selectedPresentation = useSelector((state) => state.dataviz.selectedPresentation)
    const labels = useSelector((state) => state.labels.labels)

    const dispatch = useDispatch()

    const pageName = selectedPresentation === 'Agregation' ? 'explorer' : 'competences'

    const [financeData, setFinanceData] = useState(visualisedDataTree && visualisedDataTree[selectedYear][selectedType])
    const [selectedAncestry, setSelectedAncestry] = useState(selectedNode && financeData && findAncestry(selectedNode, financeData))

    useEffect(() => {
        setFinanceData(visualisedDataTree && visualisedDataTree[selectedYear][selectedType])
    }, [selectedYear, selectedType, visualisedDataTree])

    useEffect(() => {
        const ancestry = selectedNode && financeData && findAncestry(selectedNode, financeData);
        setSelectedAncestry([].concat(ancestry && ancestry.reverse()).concat([selectedNode]))
    }, [selectedNode, financeData])

    function selectNode(node){
        dispatch(selectNodeAction(node))
    }

    return (
        visualisedDataTree && financeData ?
        (
            <div className="col-12 col-lg-5 order-1 order-lg-2">
                <div className="fil-ariane">
                    <div className="infos-budget">
                        <div className='elements'>
                            <p className='ariane-card-title'>{financeData.name.substr(financeData.name.length -1 ) === 's' ? financeData.name : financeData.name + 's'} totales:</p>
                            <p className="ariane-card-value">
                                <b><strong> { financeData.cumulatedSize && <b className='txt-large'>{financeData.cumulatedSize.toLocaleString()}</b>}</strong> €</b>
                            </p>
                            <p>{ labels[pageName + '.' + selectedType] }</p>
                        </div>
                    </div>
                    <FilArianeSiblingGroup 
                        siblings={financeData.children} 
                        selectedAncestry={selectedAncestry.slice(1)} 
                        selectNode={selectNode}
                        selectParent={() => selectNode(financeData)}
                        amountMax={financeData.cumulatedSize}
                    />
                </div>
            </div>
        ) :
        'En chargement...'
    )
}

export default FilAriane