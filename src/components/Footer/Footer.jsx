import React from 'react'

import { useSelector } from 'react-redux'

function Footer() {
    const labels = useSelector((state) => state.labels.labels)
    const urlLegalsNotices = 'https://' + window.location.host + '/mentions-legales'
    return (
        <footer>
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-12">
                        <hr/>
                        <div className="link-footer text-center">
                            <a href={ urlLegalsNotices } rel="noreferrer" target="_blank">{ labels['pied.mentions.lien.texte'] }</a>
                            <span> - </span>
                            <a href={ labels['pied.contact.lien'] } rel="noreferrer" target="_blank">{ labels['pied.contact.lien.texte'] }</a>
                            <span> - </span>
                            <a href={ labels['pied.gitlab.lien'] } rel="noreferrer" target="_blank">{ labels['pied.gitlab.lien.texte'] }</a>
                            <div>
                                Version: { process.env.APP_VERSION }
                            </div>
                        </div>
                    </div>
                </div>  
            </div> 
        </footer>
    )
}

export default Footer