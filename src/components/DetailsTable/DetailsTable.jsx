import React , {useState, useEffect} from 'react'

import { useSelector } from 'react-redux'
import { CSVLink } from 'react-csv'
import moment from 'moment'

import LineTable from './LineTable'

export default function DetailsTable() {

    const selectedNode = useSelector((state) => state.dataviz.selectedNode)
    const selectedYear = useSelector((state) => state.dataviz.selectedYear)
    const labels = useSelector((state) => state.labels.labels)

    const ASC = 'asc'
    const DESC = 'desc'
    const NATURE = 'name'
    const FONCTION = 'nameFonction'
    const BUDGET = 'size'

    const [fileName, setFileName] = useState()
    const [searchF, setSearchF] = useState('')
    const [searchN, setSearchN] = useState('')
    const [search, setSearch] = useState(['', ''])
    const [sortedDatas, setSortedDatas] = useState(null)

    const headers = [
        { label: 'bgt_nature', key: 'idNat' },
        { label: 'bgt_nature_label', key: 'name' },
        { label: 'bgt_fonction', key: 'idFonct' },
        { label: 'bgt_fonction_label', key: 'nameFonction' },
        { label: 'montant', key: 'size' }
    ]

    const sort = (data, order, onWhat) => {
        let resOne = -1
        let resTwo = 1
        if (order === ASC) {
            resOne = 1
            resTwo = -1
        }
        if (onWhat === BUDGET) {
            return data.sort( (a, b) => 
                a[onWhat] === b[onWhat] ? 0 :
                parseFloat((a[onWhat]).toString().replace(',', '.')) > parseFloat((b[onWhat]).toString().replace(',', '.')) ? resOne : resTwo)
        }
        else {
            return data.sort( (a, b) => a[onWhat] === b[onWhat] ? 0 : a[onWhat] > b[onWhat] ? resOne : resTwo)
        }
    }

    const handleSort = (order, onWhat) => {
        const local = sort([...sortedDatas], order, onWhat)
        setSortedDatas(local)
    }

    // Lorsque une des recherche est déclenchée, on met à vide l'autre champ
    const handleSearch = (event, on) => {
        if (on === NATURE) {
            setSearchN(event.target.value.toString())
            setSearchF('')
        } else {
            setSearchF(event.target.value.toString())
            setSearchN('')
        }
        setSearch([event.target.value.toString(), on])
    }

    const filter = () => {
        const local = JSON.parse(JSON.stringify(sortedDatas))
        return local.filter( (el) => 
            el.name && el.name !== "" && el.nameFonction && el.nameFonction != null && (search[1] === FONCTION ?
                el.name.toLowerCase().indexOf(search[0].toLowerCase()) >= 0 :
                el.nameFonction.toLowerCase().indexOf(search[0].toLowerCase()) >= 0) 
        )
    } 

    // Initialisation des données à afficher
    useEffect( () => {
        setSortedDatas(sort([...selectedNode.elements], DESC, BUDGET))
    }, [selectedNode])

    return (
        sortedDatas && <div>
            <div className="graph-table cards">
                <div className="histo-header" style={{ marginBottom: "10px" }}>
                    <div className="histo-header-left">
                        <h2 className="title-cards">Détails des comptes</h2>
                        <p><strong>{selectedNode.name}</strong></p>
                    </div>
                    { 
                        sortedDatas &&
                        <div className="histo-header-right">
                            <div>
                                <button className="histo-title">
                                    <CSVLink data={ filter().map( el => { el.size = el.size.toString().replace('.', ','); return el }) }
                                            separator={";"}
                                            filename={fileName}
                                            onClick={ () => setFileName("raw_data_" + moment().format('YYYYMMDD-HH:mm:ss') + '.csv')}
                                            headers={headers} >
                                        { labels['details.telecharger.lien.texte'] }
                                    </CSVLink>
                                </button>
                            </div>
                        </div>
                    }
                </div>
                <table>
                    <thead>
                        <tr>
                            <th>
                                <div className="box-filter">
                                    { labels['details.colonne.un'] }
                                    <div className="filter">
                                        <div className="arrow-bottom">
                                            <button style={{ borderStyle: "none", backgroundColor: "transparent"}} onClick={ (e) => handleSort(DESC, NATURE) } >
                                                <img src="/img/arrow-bottom.png" alt=""/>
                                            </button>
                                        </div>
                                        <div className="arrow-top">
                                            <button style={{ borderStyle: "none", backgroundColor: "transparent"}} onClick={ (e) => handleSort(ASC, NATURE) } >
                                                <img src="/img/arrow-top.png" alt=""/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div className="box-filter">
                                    { labels['details.colonne.deux'] }
                                    <div className="filter">
                                        <div className="arrow-bottom">
                                            <button style={{ borderStyle: "none", backgroundColor: "transparent"}} onClick={ (e) => handleSort(DESC, FONCTION) } >
                                                <img src="/img/arrow-bottom.png" alt=""/>
                                            </button>
                                        </div>
                                    
                                        <div className="arrow-top">
                                            <button style={{ borderStyle: "none", backgroundColor: "transparent"}} onClick={ (e) => handleSort(ASC, FONCTION) } >
                                                <img src="/img/arrow-top.png" alt=""/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                <div className="box-filter">
                                    { labels['details.colonne.trois'] + ' ' + selectedYear }
                                    <div className="filter">
                                        <div className="arrow-bottom">
                                            <button style={{ borderStyle: "none", backgroundColor: "transparent"}} onClick={ (e) => handleSort(DESC, BUDGET) } >
                                                <img src="/img/arrow-bottom.png" alt=""/>
                                            </button>
                                        </div>
                                    
                                        <div className="arrow-top">
                                            <button style={{ borderStyle: "none", backgroundColor: "transparent"}} onClick={ (e) => handleSort(ASC, BUDGET) } >
                                                <img src="/img/arrow-top.png" alt=""/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th className="search-box">
                                <input value={ searchF } className="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Recherche" onChange={ (e) => handleSearch(e, FONCTION) } />
                                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">
                                </button>
                            </th>
                            <th className="search-box">
                                <input value={ searchN } className="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Recherche" onChange={ (e) => handleSearch(e, NATURE) } />
                                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">
                                </button>
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            filter().map( (el, index) => {
                                return (
                                    <LineTable key={index} item={el} />
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}