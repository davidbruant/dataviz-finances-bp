import React from 'react'

export default function LineTable( {item} ) {
    const numberFormat = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR', minimumFractionDigits: 0 })
    return (
        <tr>
            <td>
                <p title={ item.idNat + ' - ' + item.name }>{ item.idNat } - { item.name }</p>
            </td>
            <td>
                <p title={ item.idFonct + ' - ' + item.nameFonction }>{ item.idFonct } - { item.nameFonction }</p>
            </td>
            <td>
                <p>{ item.size && numberFormat.format(item.size) }</p>
            </td>
        </tr>
    )
}