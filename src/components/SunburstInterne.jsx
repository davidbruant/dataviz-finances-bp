// This is a modified copy of https://github.com/NCarson/react-zooming-sunburst/blob/master/src/Sunburst.js
// BSD-3-Clause licence https://github.com/NCarson/react-zooming-sunburst/blob/19cb596ee4865ce3aced631881842b6a7b507599/package.json#L14

import React from "react";
import PropTypes from "prop-types";
import shallowEqual from "shallowequal";

import { select as d3Select } from "d3-selection";
import {
  scaleLinear as d3ScaleLinear,
  scaleSqrt as d3ScaleSqrt
} from "d3-scale";
import {
  hierarchy as d3Hierarchy,
  partition as d3Partition
} from "d3-hierarchy";
import { arc as d3Arc } from "d3-shape";
import { path as d3Path } from "d3-path";
import { interpolate as d3Interpolate } from "d3-interpolate";


// We have to import this event though we dont use it
import { transition as d3Transition } from "d3-transition";

d3Transition();

/* REFS
 * zoomable /w/ labels -- https://bl.ocks.org/vasturiano/12da9071095fbd4df434e60d52d2d58d
 * text opacity -- https://gist.github.com/metmajer/5480307
 */

/**
* Creates a zoomable Sunburst
* @param {object} props
* @param {object} props.data - see the d3 {@link https://github.com/defunctzombie/d3-examples/blob/master/dendrogram/flare.json|flare.json}
    data for the shape that is required. 
* @param {string} props.width - width of svg
* @param {string} props.height - height of svg. 
*   If width and height are not the same there will be dead space.
* @param {number} props.count_member - what data element to use for slice size
* @param {number} [props.radianCutoff=.01] - smallest slice to show in radians
* @param {number} [props.transitionDuration=500] - ms for animation
* @param {number} [props.font_size=12] - for calculating if text fits
* @param {func} [props.colorFunc=(node, current_color) => current_color]
        - Custom color func for slices with heights > 0.
* @param {func} [props.labelFunc] - returns text to slice
* @param {string} [props.domID] - will be random if undefined
* @param {func} [props.onMouseover]
* @param {func} [props.onMouseout]
* @param {func} [props.onClick]
* @param {string} [props.key_member] - data member to construct dom ids from
*/
//FIXME normalize function signatures
//FIXME normalize case
class Sunburst extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    count_member: PropTypes.string.isRequired,

    // requried /w/ default
    radianCutoff: PropTypes.number.isRequired, // smallest slice to show in radians
    transitionDuration: PropTypes.number.isRequired, // ms for animation
    font_size: PropTypes.number.isRequired, // for calculating if text fits

    colorFunc: PropTypes.func, // custom colorizing for slice

    domId: PropTypes.string, // will be random if undefined
    onMouseover: PropTypes.func,
    onMouseout: PropTypes.func,
    onClick: PropTypes.func,
    labelFunc: PropTypes.func, // returns text for slice
    key_member: PropTypes.string, // unique id
    _debug: PropTypes.bool,
    
    _log: PropTypes.func,
    _warn: PropTypes.func
  };

  static defaultProps = {
    radianCutoff: 0.001,
    transitionDuration: 500,
    colorFunc: data => data.color,
    key_member: "id",
    font_size: 12,
    _debug: false,
    _log: console.log,
    _warn: console.warn,
    
  };

  constructor(props) {
    super(props);
    
    this.radius = (Math.min(this.props.width, this.props.height) / 2) - 20;
    // x is angle
    this.x = d3ScaleLinear().range([0, 2 * Math.PI]);
    // y is radius
    this.y = d3ScaleSqrt().range([0, this.radius]);


    this.arc = d3Arc()
      .startAngle(d => {
        return Math.max(0, Math.min(2 * Math.PI, this.x(d.x0)));
      })
      .endAngle(d => {
        return Math.max(0, Math.min(2 * Math.PI, this.x(d.x1)));
      })
      .innerRadius(d => {
        return Math.max(0, this.y(d.y0)) + 2;
      })
      .outerRadius(d => {
        return Math.max(0, this.y(d.y1));
      })
      .cornerRadius(d => 3)
      .padAngle(0.01);

    this.partition = d3Partition();

    this.domId =
      this.props.domId ||
      "sunburst-wrapper-" + Math.round(Math.random() * 1e12).toString();
    this.svg = undefined;
    this.selectedElement = undefined;
  }

  componentDidMount() {
    this.props._debug && this.props._log("Sunburst: componentDidMount()");

    this._createSVG(this.props.data);
    this._update(this.rootD3HierarchyData);
  }

  shouldComponentUpdate(nextProps) {
    this.props._debug &&
      this.props._log("Sunburst: shouldComponentUpdate()", this.props);
    if (!shallowEqual(this.props, nextProps)) {
      return true;
    }
    return true;
  }

  _createSVG(financeDataTree){
    this.rootD3HierarchyData = d3Hierarchy(financeDataTree)

    const rootD3Data = this.rootD3HierarchyData
      .sum(d => {
        if (d[this.props.count_member] === undefined)
          this.props._warn(
            `props.count_member (${
              this.props.count_member
            }) is not defined on data`
          );
        return !d.children || d.children.length === 0
          ? d[this.props.count_member]
          : 0;
      });


    const data = this.partition(rootD3Data)
      .descendants();

    
    const w = this.props.width;
    const h = this.props.height;
    const el = d3Select("#" + this.domId);

    this.svg = el.append("svg");
    this.svg
      .style("class", "sunburst-svg")
      .style("width", w + "px")
      .style("height", h + "px")
      .attr("viewBox", `${-w / 2} ${-h / 2} ${w} ${h}`);

    /*
      mouseover on top svg element removes the tooltip on capture phase of the event
    */
    this.svg.on("mouseover", d => {
      this.tooltipD3Element
        .style("display", "none");
    }, true);

    var gSlices = this.svg
      .selectAll("g")
      .data(data)
      .enter()
      .append("g");
    
    gSlices.exit().remove();

    const key = this.props.key_member;

    gSlices
      .append("path")
      .attr("class", d => {
        return `sunburst-main-arc`;
      })
      .attr("id", (d, i) => {
        return key ? `mainArc-${d.data[key]}` : `mainArc-${i}`;
      })
      .style("fill", d => (d.parent ? d.data.color : "white"))
      .on("click", (event, node) => {
          this.select(node)
      })
      .on("mouseover", (event, d) => {
        const [x, y] = this.arc.centroid(d)
        //creating ellipsis if text name lenght is > 95 (because of svg tooltip3delement already created)
        const nameElement = d.data.name.length > 40 ? 
        d.data.name.substring(0, 40) + ' ...' : d.data.name

        const tooltipHTMLContent = `<span>${nameElement}</span>
        <span>${d.data.cumulatedSize.toLocaleString()}&nbsp;€</span>`

        this.tooltipD3Element
          .style("display", "initial")

        const tooltipX = x < 0 ? x + 100 : x - 50
        const tooltipY = y < 0 ? y + 50 : y - 100
        this.tooltipD3Element
          .select("foreignObject")
          .attr('x', tooltipX)
          .attr('y', tooltipY);

        this.tooltipD3Element
          .select("foreignObject div")
          .html(tooltipHTMLContent);
      });
    
    gSlices
      .append("svg:path")
      .attr("class", "sunburst-hidden-arc")
      .attr("id", (_, i) => `hiddenArc${i}`)
      .attr("d", this._middleArcLine.bind(this))
      .style("fill", "none");

    this.linkToParent = this.svg
      .append("svg:a")
      .attr("class", "sunburst-link-to-parent")
      .on("click", () => {
        const parent = this.selectedElement.parent;
        if(parent)
          this.select(parent)
      })
    
    this.linkToParent
      .style("display", "none")
      .append("foreignObject")
      .attr("width", 100)
      .attr("height", 100)
      .attr('transform', "translate(-50, -50)")
      .append("xhtml:div");

    const buttonParentContent = `<span class="long-arrow-left"></span>
    <span class="button-label">niveau supérieur</span>`

    this.linkToParent
      .select("foreignObject div")
      .html(buttonParentContent);

    this.tooltipD3Element = this.svg
      .append('g')
      .attr("class", "sunburst-piece-tooltip");
    
    this.tooltipD3Element
      .append("foreignObject")
      .attr("width", 120)
      .attr("height", 120)
      .attr('transform', "translate(-100, -50)")
      .append("xhtml:div")

  }

  _destroy_svg() {
    this.props._debug && this.props._log("Sunburst: _destroy_svg()");
    this.svg && this.svg.remove();
    this.svg = undefined;
  }

  componentDidUpdate(prevProps) {
    //prevProps
    this.props._debug && this.props._log("Sunburst: componentDidUpdate()");

    if(prevProps.data !== this.props.data){
      this._destroy_svg()
      this._createSVG(this.props.data)
    }

    const selectedElementFromProps = this.props.selectedNode;

    const d3SelectedElement = this.rootD3HierarchyData.find(d => {
      return d.data.id === selectedElementFromProps.id
    })


    if(d3SelectedElement)
      this._update(d3SelectedElement);
  }

  componentWillUnmount() {
    this.props._debug && this.props._log("Sunburst: componentWillUnmount()");
    this._destroy_svg();
  }
  /**
   * Programatically select a slice.
   * @param node the selected node in d3-hierarchy-partition data format
   */
  select(node) {
    this.props.onClick && this.props.onClick(node);
    this._update(node);
  }


  _update(d, i, a) {
    this.props._debug && this.props._log("Sunburst: _update(d, i, a)");

    // ignore clicks to the already-selected element
    if (this.selectedElement === d) return;

    this.selectedElement = d;

    const referenceDepth = d.depth;

    this.linkToParent.style("display", referenceDepth !== 0 ? "initial" : "none");

    const transition = this.svg
      .transition()
      .duration(this.props.transitionDuration) // duration of transition
      .tween("scale", () => {
        const xd = d3Interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Interpolate(this.y.domain(), [d.y0, 1]),
          yr = d3Interpolate(this.y.range(), [d.y0 ? 20 : 0, this.radius]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t)).range(yr(t));
        }
      });

    transition
      .selectAll("path.sunburst-hidden-arc")
      .attrTween("d", d => () => this._middleArcLine(d))

    transition
      .selectAll("path.sunburst-main-arc")
      .attr('data-height', d => d.height)
      .attr('data-data-depth', d => d.depth)
      .attr('data-current-depth', d => d.depth - referenceDepth)
      .attrTween("d", d => () => {
        const arc = this.arc(d);
        return arc;
      })

  }

  _middleArcLine(d) {
    this.props._debug && this.props._log("Sunburst: _middleArcLine(d)");
    const halfPi = Math.PI / 2;
    const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);

    const middleAngle = (angles[1] + angles[0]) / 2;
    const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw
    if (invertDirection) {
      angles.reverse();
    }

    const path = d3Path();
    path.arc(0, 0, r, angles[0], angles[1], invertDirection);
    return path.toString();
  }

  // we have to render first then componentMounted will give us
  // access to the dom
  render() {
    this.props._debug && this.props._log("Sunburst: render()");
    
    return <div className="sunburst-wrapper" id={this.domId} />
  }

}

export default Sunburst;
