//@ts-check

import React from 'react'

import { useSelector } from 'react-redux'


function DataTable({data: elements, type, name}) {

    // @ts-ignore
    const selectedPresentation = useSelector((state) => state.dataviz.selectedPresentation)
    const labels = useSelector((state) => state.labels.labels)

    const label = labels ?
        (selectedPresentation === 'Agregation' ? labels['tableau.titre.comptes'] : labels['tableau.titre.competences']) 
        : 'en chargement...'

    const years = elements && elements.map(({year}) => year)

    const amountByYearById = new Map()
    // saving all name-related infos to build the name later in case there are name collisions
    const nameInfoById = new Map()

    const nameById = new Map()

    if(elements){

        for(const {year, stackedChildren} of elements){
            for(const stackedChild of stackedChildren){
                const id = stackedChild.node.id
                if(id){
                    if(!nameInfoById.has(id)){
                        nameInfoById.set(id, stackedChild.node)
                    }

                    let amountByYear = amountByYearById.get(id)

                    if(!amountByYear){
                        amountByYear = new Map()
                    }

                    amountByYear.set(year, stackedChild.node.cumulatedSize)

                    amountByYearById.set(id, amountByYear)
                }
            }
        }

        // remove rows in which all amounts are 0
        for(const [id, amountByYear] of amountByYearById){
            if([...amountByYear.values()].every(amount => amount === 0)){
                amountByYearById.delete(id)
            }
        }

        // trying to make nameById
        for(const [id, {name}] of nameInfoById){
            nameById.set(id, name)
        }
        // adjust if there are duplicates
        if(nameById.size > (new Set(nameById.values())).size){
            // there are duplicates
            // append the Fonction to all names

            for(const [id, {name, Fonction}] of nameInfoById){
                nameById.set(id, `${name} (F${Fonction})`)
            }
        }

    }

    const SUBTITLE_SHEET = type === null ? name : (type + ' ' + name)

    return (
        <div className="container-fluid container-sm p-0 p-md-3 p-sm-2">
                <div className="graph-table table-donnee cards">
                    <div className="content-cards">	
                        <h2 className="title-cards">{ label }</h2>
                        <p><strong>{SUBTITLE_SHEET}</strong></p>
                    </div> 
                    <div className="table-responsive">
                        <table>
                            <thead> 
                                <tr>
                                    <th>&nbsp;</th>
                                    { years.map( year => <th key={year}>{year}</th> ) }
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    [...amountByYearById.entries()].map(([id, amountByYear]) => {
                                        return (
                                            <tr key={id}>                                                
                                                <td title={ nameById.get(id) }><p><b>{ nameById.get(id) }</b></p></td>
                                                {
                                                    years.map(year => {
                                                        return <td key={year}>
                                                            <p>{ (amountByYear.get(year) || 0).toLocaleString() + "€"} </p>
                                                        </td>
                                                    })
                                                }
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        
                        </table>
                    </div>
                </div>
            </div>
    )
}

export default DataTable
