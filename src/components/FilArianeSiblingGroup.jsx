import React from 'react'
import { useSelector } from 'react-redux'

import { Tooltip as ReactTooltip } from "react-tooltip"

export default function FilArianeSiblingGroup({siblings, selectedAncestry, selectNode, selectParent, amountMax, isSubLevel = false }) {

    const labels = useSelector((state) => state.labels.labels)
    const selectedPresentation = useSelector((state) => state.dataviz.selectedPresentation)
    const pageName = selectedPresentation === 'Agregation' ? 'explorer' : 'competences'

    function handleClickNode(sibling) {
        if (sibling === selectedAncestry.at(-1)){
            selectParent()
        } else {
            selectNode(sibling)
        }
    }

    return <>
    {
        [...siblings].filter( el => el.cumulatedSize > 0 ).sort((a, b) => b.cumulatedSize - a.cumulatedSize).map((sibling, index) => {
            const isSelected = sibling.id === (selectedAncestry && selectedAncestry[0] && selectedAncestry[0].id)
            const hasChildren = !!sibling.children

            return <div key={index + '-' + sibling.id}>
                <button key={sibling.id} onClick={() => handleClickNode(sibling)} 
                    className={`last-collapse btn btn-link btn-block text-left ${hasChildren ? (isSelected ? "btn-collapse" : "btn-collapse collapsed") : ""} ${isSelected ? "selected" : ""}`} 
                    type="button" data-toggle="" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"
                >
                    <span className="title-collapse" data-title={sibling.name}> {sibling.name} </span>
                    <span className="budget">{ sibling.cumulatedSize.toLocaleString()}&nbsp;€</span>
                </button>
                <div className="progress-box">
                        {isSubLevel && 
                            <div className="accordion-info">
                                <button data-tooltip-id={sibling.id} data-tooltip-content={labels['breadcrumbs.tooltip.info']} className="btn btn-secondary btn-tooltips">
                                    <img src="/img/information-button.png" alt="" />
                                </button>

                                <ReactTooltip id={sibling.id} 
                                    place="right" 
                                    style={ {color: 'black', backgroundColor: 'white', zIndex: 5} }
                                    effect="solid"
                                />
                                    
                            </div>
                        }
                    <div className="progress">
                        <div className="progress-bar" role="progressbar" aria-valuenow={((Math.floor(sibling.cumulatedSize) / amountMax)*100).toFixed(2) } aria-valuemin="0" aria-valuemax="100" style={{width:(Math.floor(sibling.cumulatedSize)/ amountMax)*100+'%', background: sibling.color}}></div>
                    </div>
                </div>
                {
                    isSelected && 
                        <div className="accordion" id="accordionOne">
                            <div className="card">
                                <div className="card-header second-accordion" id="headingOne">
                                    { labels[pageName + '.' + sibling.deepId.replace('racine.', '')] !== undefined &&
                                        <p className='card-description'>{ labels[pageName + '.' + sibling.deepId.replace('racine.', '')] }</p>
                                    }
                                    { hasChildren &&
                                        <p className='card-description'>{ labels['breadcrumbs.label'] } { sibling.name }</p>
                                    }

                                    {
                                        Array.isArray(sibling.children) &&
                                            <FilArianeSiblingGroup 
                                                siblings={sibling.children} 
                                                selectedAncestry={selectedAncestry.slice(1)} 
                                                selectNode={selectNode}
                                                selectParent={() => selectNode(sibling)}
                                                isSubLevel={true}
                                                amountMax={amountMax}
                                            />
                                    }
                                </div>
                            </div>
                        </div>
                }
    
            </div>
        })
    }
        </>
}
