//@ts-check

import React from 'react'

import { useSelector, useDispatch } from 'react-redux'
import { selectNode } from '../redux/datavizSlice'

import useWindowDimensions from '../hooks/useWindowDimensions'
import SunburstInterne from "./SunburstInterne"

export default () => {
    /** @type { AugmentedAggregationTree } */
    // @ts-ignore
    const visualisedDataTree = useSelector((state) => state.dataviz.visualisedDataTree)
    /** @type { number } */
    // @ts-ignore
    const selectedYear = useSelector((state) => state.dataviz.selectedYear)
    /** @type { 'D' | 'R' } */
    // @ts-ignore
    const selectedType = useSelector((state) => state.dataviz.selectedType)
    /** @type { AugmentedAggregationTree } */
    // @ts-ignore
    const selectedNode = useSelector((state) => state.dataviz.selectedNode)
    
    const dispatch = useDispatch()

    const handleClickNode = (evt) => {
        dispatch(selectNode(evt.data))
    }

    const { height, width } = useWindowDimensions()

    const sunburstWidth = (width < 1360 ? (width * 50 / 100) : 730)

    return (
        <>
            <h2 className="title-cards" style={{fontSize: 24, fontWeight: 700}}>Répartition des budgets</h2> {/*To Wrap under a component */}
            <p>{selectedNode && <strong>{selectedNode.name}</strong>}</p>
            <div className="col-10 col-8 col-lg-4 col-md-5 col-sm-8 col-xl-3 p-md-0 pl-0 pr-0 sticky-box-options">
            </div>
            
            <div className='wrapper'>
                {
                    visualisedDataTree && selectedNode ?
                        (
                        <SunburstInterne
                            onClick={ handleClickNode }
                            data={visualisedDataTree[selectedYear][selectedType]}   // Data as prop to Sunburst Schema:{name : "" , children :[]}
                            selectedNode={ selectedNode }
                            width={ sunburstWidth }
                            height={ sunburstWidth }
                            count_member="cumulatedSize"
                            labelFunc={node => node.data.name}
                            colorFunc = {node => node.data.color}
                            _debug={false}
                        />
                        ) : 
                        'En chargement...'
                }
            </div>
        </>
    )
}
