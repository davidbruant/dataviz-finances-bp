import React from "react"

import { Bar } from "victory"

class CustomBar extends React.Component {
  render() {
    const spaceBetween = 1
    const y0 = this.props.y0 - spaceBetween
    return (
      <Bar className="inverted-border-radius" {...this.props} y0={y0} />)
  }
}

export default CustomBar