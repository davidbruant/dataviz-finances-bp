//@ts-check

import React, { useState, useEffect } from 'react'

import { useSelector, useDispatch } from 'react-redux'

import { selectNode } from '../../redux/datavizSlice'

import {isAugmentedAggregationNode} from '../../predicates.js'

import { VictoryChart, VictoryStack, VictoryBar, VictoryTooltip, VictoryAxis, VictoryTheme, Bar } from 'victory'

import ModalPerso from '../ModalPerso/ModalPerso'
import useModal from '../../hooks/useModal'
import CustomLegend from '../CustomLegend/CustomLegend'
import { findInTreeByDeepId } from '../../utils/common'
import CustomBar from './CustomBar'

import DataTable from '../DataTable/DataTable'

const TOOLTIP_FONT_SIZE = 7
const TICK_LABELS_FONT_SIZE = 7

function makeStackedBarChartsLegend(stackedBarChartData){ 

    const barChartLegentByName = new Map()

    for(const {stackedChildren} of stackedBarChartData){
        for(const {node: {name, color}} of stackedChildren){
            barChartLegentByName.set(
                name,
                {name, symbol: {fill: color}}
            )
        }
    }

    return [...barChartLegentByName.values()]
}



export default function StackedBarChart() {
    // @ts-ignore
    const visualisedDataTree = useSelector((state) => state.dataviz.visualisedDataTree)
    // @ts-ignore
    const selectedNode = useSelector((state) => state.dataviz.selectedNode)
    /** @type {AvailableYears} */
    // @ts-ignore
    const availableYears = useSelector((state) => state.dataviz.availableYears)
    /** @type {number} */
    // @ts-ignore
    const selectedYear = useSelector((state) => state.dataviz.selectedYear)
    // @ts-ignore
    const selectedType = useSelector((state) => state.dataviz.selectedType)
    //@ts-ignore
    const labels = useSelector((state) => state.labels.labels) || {}

    const dispatch = useDispatch()

    const [dataStackChartRaw, setDataStackChartRaw] = useState([])
    /** @type { [ {year: number, stackedChildren: {y0: number?, node: AugmentedAggregationTree}[] }[], any] } */
    const [dataStackChart, setDataStackChart] = useState([])
    const [maxScaleAxis, setMaxScaleAxis] = useState(0)
    const [dataLegend, setDataLegend] = useState([])
    
    const LABEL_TYPE_COMPTE = (
            selectedNode && selectedNode.name !== 'Dépenses' && selectedNode.name !== 'Dépense' && selectedNode.name !== 'Recettes' && selectedNode.name !== 'Recette'
        ) ? (selectedType === "D" ? "Dépenses" : "Recettes")
    : null

    const selectNodeOnStackedBar = (node) => {
        console.log('selectNodeOnStackedBar', node)
        
        dispatch(selectNode(node))
    }

    useEffect(() => {

        const stackedBarChartData = availableYears && selectedNode ? 
            availableYears.slice(-5).map(year => {
                const yearlySelectedNode = findInTreeByDeepId(selectedNode.deepId, visualisedDataTree[year][selectedType]);

                return { 
                    year, 
                    
                    // if yearlySelectedNode has children, represent stacked bar with children, otherwise the element itself
                    stackedChildren: !yearlySelectedNode ? [{y0: 0, node: {cumulatedSize: 0}}] : (  
                        isAugmentedAggregationNode(yearlySelectedNode) ? yearlySelectedNode.children : [yearlySelectedNode]
                    ).map(node => ({node, y0: NaN}))
                };
            }) : 
            []

        // Il faut retravailler les données...
        // Pour pouvoir faire l'empillement des barres, il faut que les données pour deux ans soient comme ça :
        // [
        //   { x: 2021, y0: 0 y: 500 }, { x: 2021, y0: 500, y: 10 }, { x: 2022, y0: 510, y: 10 },
        //   { x: 2022, y0: 0 y: 100 }, { x: 2022, y0: 100, y: 200 }, { x: 2022, y0: 300, y: 100 }
        // ]
        // x = colonne de la barre, y0 = départ de la barre, y = taille de la barre
        let maxScaleAxis = 0
        for(const column of stackedBarChartData){
            // Tri par ordre décroissant
            column.stackedChildren.sort((a, b) => b.node.cumulatedSize - a.node.cumulatedSize)
    
            // Le y0 stocke la somme des éléments de la barre
            let y0 = 0
            for(const child of column.stackedChildren){
                child.y0 = y0;
                y0 += child.node.cumulatedSize;
            }
            
            maxScaleAxis = y0 > maxScaleAxis ? y0 : maxScaleAxis
        }


        // @ts-ignore
        setDataLegend(makeStackedBarChartsLegend(stackedBarChartData))
        // On stocke les données brutes qui doivent être affichées dans la popup
        setDataStackChartRaw(JSON.parse(JSON.stringify(stackedBarChartData)))

        for(const column of stackedBarChartData){

            if(column.stackedChildren.length >= 2){
                // filtre les éléments qui sont inférieur à 1% du montant du regroupement courant
                column.stackedChildren = column.stackedChildren.filter(({node}) => {
                    return node.cumulatedSize > (maxScaleAxis * 1/100)
                })
            }
        }

        // @ts-ignore
        setDataStackChart(stackedBarChartData)

        const numberDigits = Math.round(maxScaleAxis).toString().length
        maxScaleAxis = numberDigits > 3 ? 
                            Math.ceil(maxScaleAxis/(Math.pow(10, numberDigits - 3))) * Math.pow(10, numberDigits - 3) : 
                            numberDigits > 2 ?
                                Math.ceil(maxScaleAxis/(Math.pow(10, numberDigits - 2))) * Math.pow(10, numberDigits - 2) :
                                maxScaleAxis
        setMaxScaleAxis(maxScaleAxis)
    }, [selectedNode, selectedYear, selectedType, availableYears, visualisedDataTree])

    const { isShowing: isModalDataTableShowed, toggle: toggleModalDataTable } = useModal()

    return (
        selectedNode ?
        (
            <div>
                <div className='histo-header'>
                    <h2 className="title-cards" style={{fontSize: 24, fontWeight: 700}}>{ labels['explorer.compte.histogramme.titre'] }</h2>
                    <button className="histo-title modal-toggle" onClick={toggleModalDataTable}>{ labels['explorer.compte.histogramme.lien.texte'] }</button>
                    <ModalPerso
                        isShowing={isModalDataTableShowed}
                        hide={toggleModalDataTable}
                        title="Données en tableau"
                        >
                        <DataTable data={dataStackChartRaw} type={LABEL_TYPE_COMPTE} name={selectedNode.name}/>
                    </ModalPerso>
                </div>

                <p><strong>{selectedNode.name}</strong></p>

                <VictoryChart 
                    domainPadding={{x: 40, y: 0}} theme={VictoryTheme.material} padding={{ left: 60, right: 20, top: 20, bottom: 50 }}
                    style={{ parent: { borderRadius: "30px" },
                    background: { fill: "#FFFFFF" }}} 
                >
                    <VictoryAxis
                        tickValues={dataStackChart.map((item, i) => { return [i] })}
                        style={{ tickLabels: { fontSize: TICK_LABELS_FONT_SIZE } }}
                    />
                    <VictoryAxis
                        tickValues={[maxScaleAxis / 4, maxScaleAxis / 2, maxScaleAxis * 0.75, maxScaleAxis]} dependentAxis tickFormat={x => `${x.toLocaleString()} €`}
                        style={{ tickLabels: { fontSize: TICK_LABELS_FONT_SIZE }}}
                    />
                    {
                        dataStackChart.map((column, i) => {

                            return (
                                <VictoryStack key={i} >
                                    {
                                        column.stackedChildren.map(({node, y0}, j) => {
                                            return (
                                                
                                                <VictoryBar
                                                    cornerRadius={{ topLeft: 4, topRight: 4 }}
                                                    dataComponent={ j === 0 ? <Bar /> : <CustomBar />}
                                                    style={{ data: { fill: node.color, width: 40, opacity: (column.year === selectedYear) ? 1 : 0.3 } }}
                                                    data={[{ x: column.year.toString(), y: node.cumulatedSize, y0 }]}
                                                    key={j}
                                                    labels={[node.name + " : " + node.cumulatedSize.toLocaleString() + " €"]}
                                                    labelComponent={
                                                        <VictoryTooltip 
                                                            orientation={"right"} 
                                                            pointerLength={5} 
                                                            style={[{fontSize: TOOLTIP_FONT_SIZE}]} 
                                                            flyoutHeight={30} 
                                                            flyoutStyle={{ fill: "white", stroke: "black", strokeWidth: 0.5 }} 
                                                            flyoutWidth={TOOLTIP_FONT_SIZE*(node.name + " : " + node.cumulatedSize.toLocaleString()).length/2 + 10} 
                                                        />
                                                    }
                                                    events = {[
                                                        {target:"data" ,
                                                            eventHandlers:{
                                                                onClick : (e)=> {
                                                                    e.preventDefault()
                                                                    selectNodeOnStackedBar(node)
                                                                }                                                        
                                                            }
                                                        }
                                                        
                                                    ]}
                                                />
                                                
                                            )
                                        })
                                    }
                                </VictoryStack>
                            )
                        })
                    }
                </VictoryChart>
                <CustomLegend data={dataLegend}/>
            </div>
        ) : 
        'En chargement...'
    )
}