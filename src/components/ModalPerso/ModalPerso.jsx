import React from "react"
import ReactDOM from "react-dom"
import PropTypes from "prop-types"
const ModalPerso = ({ isShowing, hide, title, ...props }) =>
  isShowing
    ? ReactDOM.createPortal(
        <>
          <div className="modal-perso-overlay">
            <div className="modal-perso-wrapper">
              <div className="modal-perso">
                <div className="modal-perso-header">
                  <h4>{title}</h4>
                  <button
                    type="button"
                    className="modal-perso-close-button"
                    onClick={hide}
                  >
                    <span>&times;</span>
                  </button>
                </div>
                <div className="modal-perso-body">{props.children}</div>
              </div>
            </div>
          </div>
          <style jsx="true">{`
            .modal-perso-overlay {
              position: fixed;
              top: 0;
              left: 0;
              width: 100vw;
              height: 100vh;
              z-index: 1040;
              background-color: rgba(0, 0, 0, 0.5);
            }
            .modal-perso-wrapper {
              position: fixed;
              top: 0;
              left: 0;
              z-index: 1050;
              width: 100%;
              height: 100%;
              overflow-x: hidden;
              overflow-y: auto;
              outline: 0;
              display: flex;
              align-items: center;
            }
            .modal-perso {
              z-index: 100;
              background: #fff;
              position: relative;
              margin: auto;
              border-radius: 5px;
              // max-width: 500px;
              overflow-y: scroll;
              width: 80%;
              height:90%;
              padding: 1rem;
            }
            .modal-perso-header {
              display: flex;
              justify-content: space-between;
              align-items: center;
            }
            .modal-perso-close-button {
              font-size: 1.4rem;
              font-weight: 700;
              color: #000;
              cursor: pointer;
              border: none;
              background: transparent;
            }
          `}</style>
        </>,
        document.body
      )
    : null

ModalPerso.propTypes = {
    isShowing: PropTypes.bool.isRequired,
    hide: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired
}

export default ModalPerso