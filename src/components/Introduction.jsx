import React from 'react'
import { Link } from 'react-router-dom'

import { useSelector } from 'react-redux'

function Introduction() {

    const labels = useSelector((state) => state.labels.labels)
    const selectedPresentation = useSelector((state) => state.dataviz.selectedPresentation)
    const pageName = selectedPresentation === 'Agregation' ? 'explorer' : 'competences'

    return (
        <div className="container">
            <div className="headline">
                <div className="row align-items-center">
                    <div className="col-12 col-lg-8">
                        <h1>{ labels[pageName + '.compte.titre'] }</h1>
                        <p dangerouslySetInnerHTML={{ __html: labels[pageName + '.compte.texte'] }}/>
                    </div>
                    <div className="col-12 col-lg-4 col-md-8 offset-lg-0 offset-md-2">
                        <div className="cards">
                            <div className="align-items-center justify-content-center row">
                                <div className="align-items-center col-3 col-md-3 col-sm-2 col-xl-3 d-flex justify-content-center mb-0 mb-4 mb-sm-0 mb-xl-0 pr-0 pr-lg-0 pr-sm-1">
                                    <img src="/img/un-peu-perdu.png" alt="{ labels['introduction.titre'] }" title="{ labels['introduction.titre'] }"/>
                                </div>
                                <div className="align-items-sm-start align-items-xl-start col-8 col-md-9 col-sm-10 col-xl-9 d-flex flex-column justify-content-center justify-content-sm-end">
                                    <p className="txt-large-lowcase mb-1">
                                        <b>{ labels['introduction.titre'] }</b>
                                    </p>
                                    <p className="mb-0 mb-sm-2 text-sm-left text-xl-left">{ labels['introduction.texte'] }</p>
                                </div>
                                <div className="col-12 col-lg-12 col-md-9 col-sm-10 col-xl-9 d-flex flex-column offset-lg-0 offset-md-3 offset-sm-2 offset-xl-0 offset-xl-3">
                                    <Link to="/budget" className="btn btn-primary w-100">{ labels['introduction.bouton.texte'] }</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Introduction