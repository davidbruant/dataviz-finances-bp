//@ts-check

import React from 'react'
import { Link } from 'react-router-dom'
import { connect, useSelector } from 'react-redux'
import { create } from '@lottiefiles/lottie-interactivity'
import Header from '../components/Header'
import Footer from '../components/Footer/Footer'
import { getApplicationTitle } from '../utils/common'

import lottieDatas from '../../données/commun/lottieDatasCA.js'

class ExplicationCompteAdministratif extends React.Component {

    constructor(props) {
        super(props)
        this.lotties = lottieDatas.map(() => React.createRef())
    }

    componentDidMount() {
        lottieDatas.forEach(({id, actions}, i) => {
            this.lotties[i].current.addEventListener('load', function (e) {
                create({
                    mode: 'scroll',
                    player: id,
                    actions
                })
            })
        })
    }
    
    componentWillUnmount() {
        // TODO: removeEventListener
    }

    render() {
        let lottiesCount = 0
        //@ts-ignore
        const labels = this.props.labels

        document.title = getApplicationTitle(labels, 'compte-administratif.pagename')

        return (
            <div>
                <div className="main-container">
                    <Header/>
                    <div className="page-content">
                        <div className="container">
                            <div className="headline">
                                <div className="row">
                                    <div className="col-12 col-lg-7 animate__animated animate__fadeInLeft">
                                        <h1>{ labels['compte-administratif.titre'] }</h1>
                                    </div>
                                    <div
                                        className="col-12 col-lg-4 col-lg-5 col-md-8 offset-lg-0 offset-md-2 animate__animated animate__fadeInRight">
                                        <div className="cards">
                                            <div className="row">
                                                <div className=" col-12 col-xl-6 d-flex justify-content-center">
                                                    <p className="mb-xl-0 text-center text-xl-left text-small" dangerouslySetInnerHTML={{ __html: labels['bloc.donnees.brutes.texte'] }} />
                                                </div>
                                                <div className="align-item-center col-12 col-xl-6 d-flex flex-column justify-content-center pl-xl-0">
                                                    <a
                                                        href={ labels['bloc.donnees.brutes.lien'] }
                                                        className="btn m-auto btn-primary"
                                                        rel="noreferrer"
                                                        target="_blank">
                                                        { labels['bloc.donnees.brutes.lien.texte'] }
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="position-relative">
                                <div className="align-items-start justify-content-center mb-5 row">
                                    <div
                                        className="animate__animated animate__delay-1s animate__lightSpeedInRight col-12 col-lg-6 col-xl-4">
                                        <h2 className="title-2 txt-primary">{ labels['compte-administratif.partie.1.titre'] }</h2>
                                        <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.partie.1'] }} />
                                        <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.partie.2'] }} />
                                    </div>
                                    <div className="animate__animated animate__delay-05s animate__fadeInLeft col-12 col-lg-6 d-flex justify-content-center mb-4 order-lg-0">
                                        <img src="/img/img-headline.png" alt="" />
                                    </div>
                                </div>
                                <div className="align-items-center justify-content-center justify-content-lg-end row container-buble-1">
                                    <div
                                        className="col-12 col-lg-5 p-0 col-md-8 col-sm-10 animate__animated animate__delay-1s animate__zoomIn">
                                        <div className="buble">
                                            <div className="txt-buble">
                                                <p className="text-center" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.partie.bulle'] }} />
                                            </div>
                                            <div className="bg-buble">
                                                <img src="/img/buble-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row justify-content-center row-asymetric pt-large position-relative">
                                <div id="container-draw-1">
                                    <lottie-player ref={ this.lotties[lottiesCount++] } src="/animations/0.json"
                                        background="transparent" speed="1" style={{ width: "175px", height: "175px" }} id="firstLottie">
                                    </lottie-player>
                                </div>
                                <div className="align-self-start col col-12 col-lg-4 col-md-8 justify-content-center text-center">
                                    <div className="card-calendar animFadeInLeft">
                                        <div className="card-calendar-img">
                                            <img src="/img/ca-calendar.png" alt="" />
                                        </div>
                                        <div className="card-calendar-content">
                                            <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.calendrier-1'] }}/>
                                        </div>
                                    </div>
                                    <div id="container-draw-2">
                                        <lottie-player ref={ this.lotties[lottiesCount++] } src="/animations/1.json"
                                            background="transparent" speed="1" style={{ width: "200px", height: "100px" }} id="secondLottie"
                                            mode="normal">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div
                                    className="align-self-center position-relative col col-12 col-lg-4 col-md-8 justify-content-center text-center d-flex flex-column">
                                    <div id="container-draw-3" className="order-2 order-lg-0">
                                        <lottie-player ref={ this.lotties[lottiesCount++] } src="/animations/2.json"
                                            background="transparent" speed="1" style={{ width: "191px", height: "63px" }} id="threeLottie">
                                        </lottie-player>
                                    </div>
                                    <div className="card-calendar animFadeIn animate-delay-05s order-1 flex-column">
                                        <div className="card-calendar-img">
                                            <img src="/img/ca-calendar-star.png" alt="loupe sur des documents comme métaphore de la transparence" />
                                        </div>
                                        <div className="card-calendar-content">
                                            <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.calendrier-2'] }}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="align-self-end col col-12 col-lg-4 col-md-8 justify-content-center text-center">
                                    <div className="card-calendar animFadeInRight animate-delay-05s ">
                                        <div className="card-calendar-img">
                                            <img src="/img/ca-calendar-plus.png" alt="" />
                                        </div>
                                        <div className="card-calendar-content ">
                                            <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.calendrier-3'] }}/>
                                        </div>
                                    </div>
                                    <div id="container-draw-4">
                                        <lottie-player ref={ this.lotties[lottiesCount++] } src="/animations/3.json"
                                            background="transparent" speed="1" style={{ width: "219px", height: "181px" }} id="fourLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid container-lg container-md p-0 p-md-3">
                            <div className="transparence align-items-md-end card-container-absolute justify-content-center justify-content-md-start mt-lg-large mt-xl-5 row mt-md-n5">
                                <div id="balanceOne" className="card-img-absolute d-flex justify-content-center col-12 col-lg-5 col-md-5 col-xl-4 animFadeIn">
                                    <img src="/img/transparence.svg" alt="" />
                                </div>
                                <div className="text col-12 col-lg-10 col-md-12 col-xl-6 offset-lg-1 position-md-absolute">
                                    <div className="card-rounded ca-transparence mb-md-0 animSpeedLeft">
                                        <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.transparence.1'] }}/>
                                        <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.transparence.2'] }}/>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-lg-5 mt-lg-4 position-relative row">
                                <div className="col-12 col-lg-9 offset-lg-3">
                                    <div className="align-items-center card-rounded animSpeedRight card-rounded-2 mt-5 row">
                                        <div className="col-12 col-lg-8 col-md-9 p-0">
                                            <h2 className="mb-3 txt-primary" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.titre'] }} />
                                            <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.texte'] }} />
                                        </div>
                                        <div className="align-items-center col-12 col-lg-4 col-md-3 col-sm-7 d-flex flex-md-column flex-set flex-xl-row img-resize justify-content-around mt-5 mt-md-0">
                                            <div>
                                                <img src="/img/annexes-binder.png" className="mr-lg-4" alt="" />
                                            </div>
                                            <div>
                                                <img src="/img/annexes-open-folder-in-new-tab.png" className="" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container mt-md-xlarge">
                            <div className=" justify-content-center row">
                                <div className="col-12 col-lg-5 col-md-8 col-xl-4 mb-3 animFadeInLeft ">
                                    <div className="align-items-center d-flex justify-content-center row-img">
                                        <div>
                                            <img src="/img/annexes-playground.png" className="mb-3" alt="" />
                                        </div>
                                        <div>
                                            <img src="/img/annexes-stadium.png" className="mb-3" alt="" />
                                        </div>
                                        <div>
                                            <img src="/img/annexes-school.png" className="mb-3" alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-5 col-md-8 offset-lg-1 animFadeInRight">
                                    <h2 className="txt-primary" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.patrimoine.titre'] }} />
                                    <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.patrimoine.texte'] }} />
                                </div>
                            </div>
                        </div>
                        <div className="card-full container-fluid pt-0 ">
                            <div className="container justify-content-center position-relative ">
                                <div className=" justify-content-center justify-content-lg-start row">
                                    <div className="col-12 col-lg-4 col-md-5 mb-4 mb-md-0 offset-lg-1 p-0 pb-5 pt-5 animFadeInLeft">
                                        <h2 className="text-center text-md-right txt-primary" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.dette.titre'] }} />
                                        <p className="mb-0 text-center text-md-right" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.dette.texte'] }} />
                                    </div>
                                    <div
                                        className="align-items-end col-12 col-lg-3 col-md-3 col-md-5 d-flex justify-content-center offset-lg-1 offset-md-1">
                                        <div id="balanceTwo" className="animFadeIn">
                                            <div className="align-items-center d-flex justify-content-center row-img" style={{height: '100%'}}>
                                                <img src="/img/annexes-payment-history.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container mt-md-xlarge">
                            <div className=" justify-content-center row">
                                <div className="col-12 col-lg-5 col-md-8 col-xl-4 mb-3 animFadeInLeft ">
                                    <div className="align-items-center d-flex justify-content-center row-img">
                                        <div>
                                            <img src="/img/annexes-worker.png" className="mb-3" alt="" />
                                            </div>
                                        <div>
                                            <img src="/img/annexes-permanent-job.png" className="mb-3" alt="" />
                                        </div>
                                        <div>
                                            <img src="/img/annexes-guardian.png" className="mb-3" alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-5 col-md-8 offset-lg-1 animFadeInRight">
                                    <h2 className="txt-primary" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.rh.titre'] }} />
                                    <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.annexe.rh.texte'] }} />
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-lg-8 mt-lg-5">
                                    <p className="text-center text-lg-right mt-lg-0 title-buble animSpeedLeft animate-delay-05s"
                                        dangerouslySetInnerHTML={{ __html: labels['compte-administratif.concretement.titre'] }} />
                                    <div className="align-items-center d-md-flex duble-buble flex-row justify-content-center mb-4 mb-md-0 mb-xl-5 ">
                                        <div className="buble-col-1">
                                            <div className="buble animZoomIn">
                                                <div className="txt-buble">
                                                    <p className="text-center"dangerouslySetInnerHTML={{ __html: labels['compte-administratif.concretement.bulle.1'] }} />
                                                </div>
                                                <div className="bg-buble">
                                                    <img src="/img/buble-2.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="buble-col-2">
                                            <div className="buble animZoomIn">
                                                <div className="txt-buble">
                                                    <p className="text-center"dangerouslySetInnerHTML={{ __html: labels['compte-administratif.concretement.bulle.2'] }} />
                                                </div>
                                                <div className="bg-buble">
                                                    <img src="/img/buble-3.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="buble-col-3">
                                            <div className="buble animZoomIn">
                                                <div className="txt-buble">
                                                    <p className="text-center"dangerouslySetInnerHTML={{ __html: labels['compte-administratif.concretement.bulle.3'] }} />
                                                </div>
                                                <div className="bg-buble">
                                                    <img src="/img/buble-1.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="justify-content-center row mt-4 mt-lg-large ">
                                <div
                                    className="align-items-center align-items-lg-end col-12 col-lg-4 col-sm-8 col-xl-4 d-flex flex-column order-2 text-center text-lg-right animFadeInLeft animate-delay-05s">
                                    <div className="alone-img">
                                        <img src="/img/nature-abacus.png" className="mb-3 mh-95" alt="" />
                                    </div>
                                    <div className="small-paragraphe">
                                        <p className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.selon.nature.titre'] }} />
                                        <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.selon.nature.texte'] }} />
                                    </div>
                                    <Link to="/" className="btn btn-primary">{ labels['compte-administratif.selon.nature.bouton.texte'] }</Link>
                                </div>
                                <div className="col-lg-2 col-md-12 mt-3 mt-lg-0 d-flex justify-content-center order-3 ">
                                    <div id="container-draw-10">
                                        <lottie-player ref={ this.lotties[lottiesCount++] } src="/animations/17.json"
                                            background="transparent" speed="1" style={{ width: "180px", height: "140px", maxWidth: "100%" }}
                                            id="tenLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-2 col-md-12 d-flex justify-content-center mb-lg-0 order-1 order-lg-3">
                                    <div id="container-draw-11">
                                        <lottie-player ref={ this.lotties[lottiesCount++] } src="/animations/18.json"
                                            background="transparent" speed="1" style={{ width: "179px", height: "119px", maxWidth: "100%" }}
                                            id="elevenLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div
                                    className="align-items-center align-items-lg-start col-12 col-lg-4 col-sm-8 col-xl-4 d-flex flex-column order-3 text-center text-lg-left animFadeInRight animate-delay-05s">
                                    <div
                                        className="align-items-center d-flex flex-md-row justify-content-between row-img row-img-asymetric ">
                                        <div className="align-self-md-start">
                                            <img src="/img/competences-theatre.png" className="mb-3" alt="" />
                                        </div>
                                        <div className="ml-2 mr-2 align-self-md-end">
                                            <img src="/img/competences-school-backpack.png" className="mb-3" alt="" />
                                        </div>
                                        <div className="align-self-md-start">
                                            <img src="/img/competences-handle-with-care.png" className="mb-3" alt="" />
                                        </div>
                                    </div>
                                    <div className="small-paragraphe">
                                        <p className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['compte-administratif.selon.competences.titre'] }} />
                                        <p dangerouslySetInnerHTML={{ __html: labels['compte-administratif.selon.competences.texte'] }} />
                                    </div>
                                    <Link to="/fonction" className="btn btn-primary">{ labels['compte-administratif.selon.competences.bouton.texte'] }</Link>
                                </div>
                            </div>
                        </div>
                        <div style={{ minHeight: "200px" }}/>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

/**
 * @param { {labels: Object} } state
 */
function mapStateToProps(state){
    return {labels: state.labels.labels}
}

export default connect(mapStateToProps)(ExplicationCompteAdministratif)


