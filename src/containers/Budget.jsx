//@ts-check

import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { create } from '@lottiefiles/lottie-interactivity'
import Header from '../components/Header'
import Footer from '../components/Footer/Footer'
import { getApplicationTitle } from '../utils/common'

import lottieDatas from '../../données/commun/lottieDatasBudget.js'

class Budget extends React.Component {

    SIZE = 21

    constructor(props) {
        super(props)
        this.lotties = []
        for(let i=0; i<=this.SIZE; i++) {
            this.lotties.push(React.createRef())
        }
    }

    componentDidMount() {
        lottieDatas.forEach(({id, actions}, i) => {
            this.lotties[i].current.addEventListener('load', () => {
                create({ mode: 'scroll', player: id, actions })
            })
        })
    }

    componentWillUnmount() {
        // TODO: removeEventListener
    }

    render() {
        const labels = this.props.labels

        document.title = getApplicationTitle(labels, 'construction.budget.pagename')

        return (
            <div>
                <div className="main-container">
                    <Header/>
                    <div className="page-content">
                        <div className="container">
                            <div className="headline">
                                <div className="row">
                                    <div className="col-12 col-lg-7 animate__animated animate__fadeInLeft">
                                        <h1>{ labels['construction.budget.titre'] }</h1>
                                        <p dangerouslySetInnerHTML={{ __html: labels['construction.budget.intro'] }} />
                                    </div>
                                    <div
                                        className="col-12 col-lg-4 col-lg-5 col-md-8 offset-lg-0 offset-md-2 animate__animated animate__fadeInRight">
                                        <div className="cards">
                                            <div className="row">
                                                <div className=" col-12 col-xl-6 d-flex justify-content-center">
                                                    <p className="mb-xl-0 text-center text-xl-left text-small" dangerouslySetInnerHTML={{ __html: labels['bloc.donnees.brutes.texte'] }} />
                                                </div>
                                                <div className="align-item-center col-12 col-xl-6 d-flex flex-column justify-content-center pl-xl-0">
                                                    <a
                                                        href={ labels['bloc.donnees.brutes.lien'] }
                                                        className="btn m-auto btn-primary"
                                                        rel="noreferrer"
                                                        target="_blank">
                                                        { labels['bloc.donnees.brutes.lien.texte'] }
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="position-relative">
                                <div className="align-items-center justify-content-center mb-5 row">
                                    <div className="animate__animated animate__delay-05s animate__fadeInLeft col-12 col-lg-6 d-flex justify-content-center mb-4 order-lg-0">
                                        <img src="/img/img-headline.png" alt="" />
                                    </div>
                                    <div
                                        className="animate__animated animate__delay-1s animate__lightSpeedInRight col-12 col-lg-6 col-xl-4">
                                        <h2 className="title-2 txt-primary" dangerouslySetInnerHTML={{__html: labels['budget.partie.1.titre'] }} />
                                        <p dangerouslySetInnerHTML={{__html: labels['budget.partie.1'] }} />
                                        <p dangerouslySetInnerHTML={{__html: labels['budget.partie.2'] }} />
                                    </div>
                                </div>
                                <div className="align-items-center justify-content-center justify-content-lg-start row container-buble-1">
                                    <div
                                        className="col-12 col-lg-5 p-0 col-md-8 col-sm-10 animate__animated animate__delay-1s animate__zoomIn">
                                        <div className="buble">
                                            <div className="txt-buble">
                                                <p className="text-center" dangerouslySetInnerHTML={{ __html: labels['budget.partie.bulle'] }}/>
                                            </div>
                                            <div className="bg-buble">
                                                <img src="/img/buble-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row justify-content-center row-asymetric pt-large position-relative">
                                <div id="container-draw-1">
                                    <lottie-player ref={ this.lotties[0] } src="/animations/0.json"
                                        background="transparent" speed="1" style={{ width: "175px", height: "175px" }} id="firstLottie">
                                    </lottie-player>
                                </div>
                                <div className="align-self-start col col-12 col-lg-4 col-md-8 justify-content-center text-center">
                                    <div className="card-calendar animFadeInLeft">
                                        <div className="card-calendar-img">
                                            <img src="/img/budg-calendar.png" alt="" />
                                        </div>
                                        <div className="card-calendar-content">
                                            <span className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['budget.primitif.titre'] }}/>
                                            <p dangerouslySetInnerHTML={{ __html: labels['budget.primitif.texte.1'] }}/>
                                            <p dangerouslySetInnerHTML={{ __html: labels['budget.primitif.texte.2'] }}/>
                                        </div>
                                    </div>
                                    <div id="container-draw-2">
                                        <lottie-player ref={ this.lotties[1] } src="/animations/1.json"
                                            background="transparent" speed="1" style={{ width: "200px", height: "100px" }} id="secondLottie"
                                            mode="normal">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div
                                    className="align-self-center position-relative col col-12 col-lg-4 col-md-8 justify-content-center text-center d-flex flex-column">
                                    <div id="container-draw-3" className="order-2 order-lg-0">
                                        <lottie-player ref={ this.lotties[2] } src="/animations/2.json"
                                            background="transparent" speed="1" style={{ width: "191px", height: "63px" }} id="threeLottie">
                                        </lottie-player>
                                    </div>
                                    <div className="card-calendar animFadeIn animate-delay-05s order-1 flex-column">
                                        <div className="card-calendar-img">
                                            <img src="/img/budg-calendar-plus.png" alt="" />
                                        </div>
                                        <div className="card-calendar-content">
                                            <span className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['budget.supplementaire.titre'] }} />
                                            <p dangerouslySetInnerHTML={{ __html: labels['budget.supplementaire.texte'] }}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="align-self-end col col-12 col-lg-4 col-md-8 justify-content-center text-center">
                                    <div className="card-calendar animFadeInRight animate-delay-05s ">
                                        <div className="card-calendar-img">
                                            <img src="/img/budg-calendar-star.png" alt="" />
                                        </div>
                                        <div className="card-calendar-content ">
                                            <p dangerouslySetInnerHTML={{ __html: labels['budget.global'] }} />
                                        </div>
                                    </div>
                                    <div id="container-draw-4">
                                        <lottie-player ref={ this.lotties[3] } src="/animations/3.json"
                                            background="transparent" speed="1" style={{ width: "219px", height: "181px" }} id="fourLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid container-lg container-md p-0 p-md-3">
                            <div className="align-items-md-end card-container-absolute justify-content-center justify-content-md-start mt-lg-large mt-xl-5 row mt-md-n5">
                                <div id="balanceOne" className="card-img-absolute d-flex justify-content-center col-12 col-lg-5 col-md-5 col-xl-4 animFadeIn">
                                    <lottie-player ref={ this.lotties[4] } src="/animations/4.json" background="transparent" speed="1"
                                        style={{ width: "420px", height: "320px", maxWidth: "100%" }} id="balanceLottie">
                                    </lottie-player>
                                </div>
                                <div className="col-12 col-lg-10 col-md-12 col-xl-8 offset-lg-1 position-md-absolute">
                                    <div className="card-rounded mb-md-0 animSpeedLeft">
                                        <p>
                                            <span className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['budget.structure.titre'] }} />
                                            <span dangerouslySetInnerHTML={{ __html: labels['budget.structure.texte'] }}/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-lg-5 mt-lg-4 position-relative row">
                                <div className="col-12 col-lg-9 offset-lg-3">
                                    <div className="align-items-center card-rounded animSpeedRight card-rounded-2 mt-5 row">
                                        <div className="col-12 col-lg-8 col-md-9 p-0">
                                            <h2 className="mb-3 txt-primary" dangerouslySetInnerHTML={{ __html: labels['recette.titre'] }}/>
                                            <p>
                                                <span className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['recette.soustitre'] }}/>
                                                <span dangerouslySetInnerHTML={{ __html: labels['recette.texte'] }}/>
                                            </p>
                                        </div>
                                        <div className="align-items-center col-12 col-lg-4 col-md-3 col-sm-7 d-flex flex-md-column flex-set flex-xl-row img-resize justify-content-around mt-5 mt-md-0">
                                            <div>
                                                <img src="/img/france.png" className="mr-lg-4" alt="" />
                                            </div>
                                            <div>
                                                <img src="/img/meal.png" className="" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="container-draw-5" className="d-lg-flex d-none">
                                    <lottie-player ref={ this.lotties[5] } src="/animations/5.json"
                                        background="transparent" speed="1" style={{ width: "432px", height: "71px" }} id="fiveLottie">
                                    </lottie-player>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row row-asymetric-2 position-relative">
                                <div
                                    className="align-self-start mt-4 col-12 col-lg-4 col-md-6 col-sm-8 mb-5 offset-lg-0 offset-md-3 offset-sm-2">
                                    <div className="price animZoomIn">
                                        <p className="mb-xl-0 p-2 p-lg-2 p-xl-3 pb-xl-5" dangerouslySetInnerHTML={{ __html: labels['recette.montant.intro'] }} />
                                        <p className="price-infos" dangerouslySetInnerHTML={{ __html: labels['recette.montant.texte'] }} />
                                        <span className="price-details" dangerouslySetInnerHTML={{ __html: labels['recette.montant.valeur'] }} />
                                        <div className="price-bg animBgPrice">
                                            <lottie-player ref={ this.lotties[6] } src="/animations/6.json" speed="1.5"
                                                style={{ width: "350px", height: "131px" }} id="bgLottiePrice1">
                                            </lottie-player>
                                        </div>
                                    </div>
                                </div>
                                <div id="container-draw-6">
                                    <lottie-player ref={ this.lotties[7] } src="/animations/7.json"
                                        background="transparent" speed="1" style={{ width: "313px", height: "148px" }} id="sixLottie-mob"
                                        class="d-lg-none">
                                    </lottie-player>
                                    <lottie-player ref={ this.lotties[8] } src="/animations/8.json"
                                        background="transparent" speed="1" style={{ width: "313px", height: "148px" }} id="sixLottie"
                                        class="d-none d-lg-block">
                                    </lottie-player>
                                </div>
                                <div
                                    className="align-self-end col-12 col-lg-6 col-md-8 col-xl-5 mt-large offset-lg-0 offset-lg-2 offset-md-2">
                                    <div
                                        className="align-items-center d-flex justify-content-sm-center justify-content-between justify-content-lg-around row-img animZoomIn ">
                                        <div>
                                            <img className="mb-3 mt-3" src="/img/female.png" alt="" />
                                        </div>
                                        <div>
                                            <img className="mb-3 mt-3" src="/img/sport.png" alt="" />
                                        </div>
                                        <div>
                                            <img className="mb-3 mt-3" src="/img/se-garer.png" alt="" />
                                        </div>
                                    </div>
                                    <h2 className=" txt-primary text-center animFadeDown " dangerouslySetInnerHTML={{ __html: labels['depenses.fonction.titre'] }} />
                                    <p className=" text-center mb-4 mb-sm-5 animFadeDown animate-delay-05s" dangerouslySetInnerHTML={{ __html: labels['depenses.fonction.texte'] }}/>
                                    <div className="price animZoomIn ">
                                        <p className="price-infos" dangerouslySetInnerHTML={{ __html: labels['depenses.fonction.montant.texte'] }} />
                                        <span className="price-details" dangerouslySetInnerHTML={{ __html: labels['depenses.fonction.montant.valeur'] }}/>
                                        <div className="price-bg animBgPrice">
                                            <lottie-player ref={ this.lotties[9] } src="/animations/9.json" speed="1.5"
                                                style={{ width: "350px", height: "131px" }} id="bgLottiePrice2">
                                            </lottie-player>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card-full container-fluid pt-0 ">
                            <div className="container justify-content-center position-relative ">
                                <div id="container-draw-7" className="d-md-flex d-none">
                                    <lottie-player ref={ this.lotties[10] } src="/animations/10.json"
                                        background="transparent" speed="1" style={{ width: "168px", height: "439px" }} id="sevenLottie"
                                        autoplay="">
                                    </lottie-player>
                                </div>
                                <div className=" justify-content-center justify-content-lg-start row">
                                    <div className="col-12 col-lg-4 col-md-5 mb-4 mb-md-0 offset-lg-1 p-0 pb-5 pt-5 animFadeInLeft">
                                        <h2 className="text-center text-md-right txt-primary" dangerouslySetInnerHTML={{ __html: labels['epargne.titre'] }} />
                                        <p className="mb-0 text-center text-md-right" dangerouslySetInnerHTML={{ __html: labels['epargne.texte'] }}/>
                                    </div>
                                    <div
                                        className="align-items-end col-12 col-lg-3 col-md-3 col-md-5 d-flex justify-content-center offset-lg-1 offset-md-1">
                                        <div id="balanceTwo" className="animFadeIn">
                                            <lottie-player ref={ this.lotties[11] } src="/animations/11.json"
                                                background="transparent" speed="1" style={{ width: "162px", height: "218px", maxWidth: "100%" }}
                                                id="balanceLottieTwo"> 
                                            </lottie-player>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container mt-md-xlarge">
                            <div className=" justify-content-center row">
                                <div className="col-12 col-lg-5 col-md-8 col-xl-4 mb-3 animFadeInLeft ">
                                    <div className="align-items-center d-flex justify-content-center row-img">
                                        <div>
                                            <img src="/img/net.png" className="mb-3" alt="" />
                                            </div>
                                        <div>
                                            <img src="/img/construction-outillage.png" className="mb-3" alt="" />
                                        </div>
                                        <div>
                                            <img src="/img/kindergarten.png" className="mb-3" alt="" />
                                        </div>
                                    </div>
                                    <p className="text-center" dangerouslySetInnerHTML={{ __html: labels['depenses.invest.texte.pictos'] }} />
                                </div>
                                <div className="col-12 col-lg-5 col-md-8 offset-lg-1 animFadeInRight">
                                    <h2 className="txt-primary" dangerouslySetInnerHTML={{ __html: labels['depenses.invest.titre'] }} />
                                    <p dangerouslySetInnerHTML={{ __html: labels['depenses.invest.texte'] }} />
                                    <div className="price animZoomIn mb-5 mt-5">
                                        <p className="price-infos" dangerouslySetInnerHTML={{ __html: labels['depenses.invest.montant.texte'] }}/>
                                        <span className="price-details" dangerouslySetInnerHTML={{ __html: labels['depenses.invest.montant.valeur'] }}/>
                                        <div className="price-bg animBgPrice">
                                            <lottie-player ref={ this.lotties[12] } src="/animations/12.json" speed="1.5"
                                                style={{ width: "350px", height: "131px" }} id="bgLottiePrice3">
                                            </lottie-player>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid container-lg container-md ">
                            <div className="row">
                                <div className="col-12 col-lg-10 offset-lg-1 p-0">
                                    <div className="align-items-center card-rounded row animFadeDown">
                                        <div className="col-12 col-md-7 col-xl-8 p-0 ">
                                            <h2 className="mb-3 txt-primary" dangerouslySetInnerHTML={{ __html: labels['recette.invest.titre'] }}/>
                                            <span className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['recette.invest.sous.titre'] }} />
                                            <p dangerouslySetInnerHTML={{ __html: labels['recette.invest.texte'] }} />
                                        </div>
                                        <div
                                            className="align-items-center col-12 col-md-5 col-sm-7 col-xl-4 d-flex flex-md-row justify-content-around mt-4 mt-md-0">
                                            <img src="/img/france-2.png" alt="" />
                                            <img src="/img/banking.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="mb-2 mb-lg-4 mt-lg-5 row">
                                <div
                                    className="col-12 col-lg-2 col-md-12 d-flex align-items-md-end justify-conten-md-end justify-content-center mb-md-0 offset-lg-3 order-3 order-lg-1 mt-4 mt-lg-0">
                                    <div id="container-draw-9">
                                        <lottie-player ref={ this.lotties[13] } src="/animations/13.json"
                                            background="transparent" speed="1" style={{ width: "134px", height: "118px" }} id="nineLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div
                                    className="col-10 col-lg-2 col-md-9 d-flex justify-content-end justify-content-lg-start mb-md-0 order-lg-3">
                                    <div id="container-draw-8">
                                        <lottie-player ref={ this.lotties[14] } src="/animations/14.json"
                                            background="transparent" speed="1" style={{ width: "107px", height: "114px" }} id="eightLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-4 col-md-8 justify-content-center offset-lg-0 offset-md-2 order-lg-2">
                                    <div className="price animZoomIn">
                                        <p className="price-infos" dangerouslySetInnerHTML={{ __html: labels['recette.invest.montant.texte'] }} />
                                        <span className="price-details" dangerouslySetInnerHTML={{ __html: labels['recette.invest.montant.valeur'] }} />
                                        <div className="price-bg animBgPrice">
                                            <lottie-player ref={ this.lotties[15] } src="/animations/15.json" speed="1.5"
                                                style={{ width: "350px", height: "131px" }} id="bgLottiePrice4">
                                            </lottie-player>
                                        </div>
                                    </div>
                                    <div id="container-draw-12-2">
                                        <lottie-player ref={ this.lotties[16] } src="/animations/16.json"
                                            background="transparent" speed="1" style={{ width: "56px", height: "79px" }} id="twelve2Lottie">
                                        </lottie-player>
                                    </div>
                                    <a href={ labels['recette.invest.bouton.lien'] } className="btn-primary m-auto animFadeIn animate-delay-05s" rel="noreferrer" target="_blank">
                                        { labels['recette.invest.bouton.texte'] }
                                        <span className="external-icon">
                                            <svg id="a" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17">
                                                <path
                                                    d="M13.81,17H2.12c-1.17,0-2.12-.95-2.12-2.12V3.19C0,2.01,.95,1.06,2.12,1.06H6.38V3.19H2.12V14.87H13.81v-4.25h2.12v4.25c0,1.17-.95,2.12-2.12,2.12Zm-5.63-6.69l-1.5-1.5L13.37,2.12h-3.81V0h7.44V7.44h-2.12V3.63l-6.69,6.69Z"
                                                    fill="#fff" />
                                            </svg>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-lg-8 mt-lg-5">
                                    <p className="text-center text-lg-right mt-lg-0 title-buble animSpeedLeft animate-delay-05s" dangerouslySetInnerHTML={{ __html: labels['aide.dataviz.titre'] }} />
                                    <div
                                        className="align-items-center d-md-flex duble-buble flex-row justify-content-center mb-4 mb-md-0 mb-xl-5 ">
                                        <div className="buble-col-1">
                                            <div className="buble animZoomIn">
                                                <div className="txt-buble">
                                                    <p className="text-center" dangerouslySetInnerHTML={{ __html: labels['aide.dataviz.bulle.1.texte'] }} />
                                                </div>
                                                <div className="bg-buble">
                                                    <img src="/img/buble-2.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="buble-col-2">
                                            <div className="buble animZoomIn">
                                                <div className="txt-buble">
                                                    <p className="text-center" dangerouslySetInnerHTML={{ __html: labels['aide.dataviz.bulle.2.texte'] }} />
                                                </div>
                                                <div className="bg-buble">
                                                    <img src="/img/buble-3.png" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="justify-content-center row mt-4 mt-lg-large ">
                                <div
                                    className="align-items-center align-items-lg-end col-12 col-lg-4 col-sm-8 col-xl-4 d-flex flex-column order-2 text-center text-lg-right animFadeInLeft animate-delay-05s">
                                    <div className="alone-img">
                                        <img src="/img/abacus.png" className="mb-3 mh-95" alt="" />
                                    </div>
                                    <div className="small-paragraphe">
                                        <p className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['selon.nature.titre'] }} />
                                        <p dangerouslySetInnerHTML={{ __html: labels['selon.nature.texte'] }} />
                                    </div>
                                    <Link to="/" className="btn btn-primary">{ labels['selon.nature.bouton.texte'] }</Link>
                                </div>
                                <div className="col-lg-2 col-md-12 mt-3 mt-lg-0 d-flex justify-content-center order-3 ">
                                    <div id="container-draw-10">
                                        <lottie-player ref={ this.lotties[17] } src="/animations/17.json"
                                            background="transparent" speed="1" style={{ width: "180px", height: "140px", maxWidth: "100%" }}
                                            id="tenLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-2 col-md-12 d-flex justify-content-center mb-lg-0 order-1 order-lg-3">
                                    <div id="container-draw-11">
                                        <lottie-player ref={ this.lotties[18] } src="/animations/18.json"
                                            background="transparent" speed="1" style={{ width: "179px", height: "119px", maxWidth: "100%" }}
                                            id="elevenLottie">
                                        </lottie-player>
                                    </div>
                                </div>
                                <div
                                    className="align-items-center align-items-lg-start col-12 col-lg-4 col-sm-8 col-xl-4 d-flex flex-column order-3 text-center text-lg-left animFadeInRight animate-delay-05s">
                                    <div
                                        className="align-items-center d-flex flex-md-row justify-content-between row-img row-img-asymetric ">
                                        <div className="align-self-md-start">
                                            <img src="/img/mortarboard.png" className="mb-3" alt="" />
                                        </div>
                                        <div className="ml-2 mr-2 align-self-md-end">
                                            <img src="/img/social-care.png" className="mb-3" alt="" />
                                        </div>
                                        <div className="align-self-md-start">
                                            <img src="/img/theater-masks.png" className="mb-3" alt="" />
                                        </div>
                                    </div>
                                    <div className="small-paragraphe">
                                        <p className="txt-large-primary" dangerouslySetInnerHTML={{ __html: labels['selon.competences.titre'] }} />
                                        <p dangerouslySetInnerHTML={{ __html: labels['selon.competences.texte'] }} />
                                    </div>
                                    <Link to="/fonction" className="btn btn-primary">{ labels['selon.competences.bouton.texte'] }</Link>
                                </div>
                            </div>
                            <div className="justify-content-center mb-n5 mt-5 row">
                                <div className="col-12">
                                </div>
                                <div id="container-draw-12">
                                    <lottie-player ref={ this.lotties[19] } src="/animations/19.json"
                                        background="transparent" speed="1" style={{ width: "56px", height: "79px" }} id="twelveLottie">
                                    </lottie-player>
                                </div>
                            </div>
                        </div>
                        <div className="container-fluid container-md p-0">
                            <div className="align-items-md-end card-container-absolute card-container-relative justify-content-center row">
                                <div className="col-12 col-lg-10 col-md-12 order-2 order-md-1">
                                    <div
                                        className="d-flex flex-column align-item-center card-rounded justify-content-center justify-content-xl-start mb-md-0 animFadeDown">
                                        <div className="d-flex flex-row align-item-center justify-content-center justify-content-xl-start">
                                            <div className="col-12 col-md-4 col-xl-3 d-flex justify-content-center mb-4 mb-md-0 order-md-1">
                                                <img className="mt-3" src="/img/calendar-4.png" alt="" />
                                            </div>
                                            <div className="col-12 col-md-8 order-md-1 p-0">
                                                <p dangerouslySetInnerHTML={{ __html: labels['cloture.compte.texte'] }} />
                                            </div>
                                        </div>
                                        <a href="./ca.html" className="btn btn-primary">Explorer les comptes administratifs</a>
                                    </div>
                                </div>
                                <div className="card-img-absolute col-12 col-lg-5 col-md-5 col-xl-4 d-flex order-1 order-md-2 order-md-3 justify-content-center animFadeIn"
                                    id="balanceThree">
                                    <lottie-player ref={ this.lotties[20] } src="/animations/20.json"
                                        background="transparent" speed="1" style={{ width: "430px", height: "324px", maxWidth: "100%" }}
                                        id="balanceLottieThree">
                                    </lottie-player>
                                </div>
                            </div>
                        </div>
                        <div className="container container-lg" style={{display: "none"}}>
                            <div className="row mt-5 mb-5">
                                <div className="col-12 d-flex justify-content-center mt-4 mb-4">
                                    <div className="btn-bg animFadeIn ">
                                        <a href="/nowhere" className="btn btn-primary">{ labels['cloture.compte.bouton.texte'] }</a>
                                        <div className="bg-btn" id="container-draw-13">
                                            <lottie-player ref={ this.lotties[21] } src="/animations/21.json"
                                                background="transparent" speed="1.5" style={{ width: "338px", height: "140px" }}
                                                id="bgLottieBtn">
                                            </lottie-player>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style={{ minHeight: "200px" }}/>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

/**
 * @param { {labels: Object} } state
 */
function mapStateToProps(state){
    return {labels: state.labels.labels}
}

export default connect(mapStateToProps)(Budget)

