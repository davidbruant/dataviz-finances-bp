//@ts-check

import React from "react"
import { useDispatch } from 'react-redux'

import { selectPresentation } from '../redux/datavizSlice'
import Squeleton from "./Squeleton"

/**
 * @param { {présentation: FinanceVizPrésentation} } _
 */
export default ({présentation}) => {
    const dispatch = useDispatch()

    dispatch(selectPresentation(présentation))

    return (
        // @ts-ignore
        <Squeleton />
    )
}
