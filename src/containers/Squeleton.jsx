import React, { useEffect } from "react"

import { useSelector } from 'react-redux'

import StackedBarChart from '../components/StackedBarChart/StackedBarChart'
import StickyBar from '../components/StickyBar'
import Header from '../components/Header'
import FilAriane from '../components/FilAriane'
import DetailsTable from '../components/DetailsTable/DetailsTable'
import Footer from '../components/Footer/Footer'
import { getApplicationTitle } from '../utils/common'

import Sunburst from "../components/Sunburst.jsx"
import Introduction from "../components/Introduction"

export default () => {
    
    const selectedNode = useSelector((state) => state.dataviz.selectedNode)
    /** @type {FinanceVizPrésentation} */
    const selectedPresentation = useSelector((state) => state.dataviz.selectedPresentation)
    const pageName = selectedPresentation === 'Agregation' ? 'explorer' : 'competences'
    const labels = useSelector((state) => state.labels.labels)

    useEffect(() => {
        document.title = getApplicationTitle(labels, pageName + '.pagename')
    }, [labels, pageName])

    return (
        <div>
            <div className="main-container">
                <Header />
                <Introduction />

                <div className="container">
                    <StickyBar/>
                    <div className="row mt-4">
                        <div className="col-12 p-0  pr-lg-3 pl-lg-3 col-lg-7 order-2 order-lg-1">
                            <div className="cards d-lg-block d-none graph-1">
                                <Sunburst />
                            </div>
                            <div className="graph-2 cards">
                                <StackedBarChart />
                            </div>
                            {selectedNode && selectedNode.elements && <DetailsTable/>}
                        </div>
                        <FilAriane />
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
