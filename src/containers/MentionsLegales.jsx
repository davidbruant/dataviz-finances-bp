import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import Header from '../components/Header'
import Footer from '../components/Footer/Footer'

import { getApplicationTitle } from '../utils/common'

const MentionsLegales = ()=>  {

    //@ts-ignore
    const labels = useSelector((state) => state.labels.labels) || {}
    
    useEffect(() => {
        document.title = getApplicationTitle(labels, 'mentions.titre')
    }, [])

    return (
        <div>
            <div className="main-container">
                <div className="container headline">
                    <Header/>
                    <h1>Mentions légales du site<br/><small>https://datalab.bordeaux-metrople.fr</small></h1>
                    <h2>Edition</h2>
                    <p>Le site https://datalab.bordeaux-metrople.fr est le catalogue des réutilisations publiques et ouvertes produites par Bordeaux Métropole et les communes qu’elle regroupe. Il est géré par la Direction de l’Innovation et de l’Aménagement Numérique au sein de la Direction Générale du Numérique et des Systèmes d’Information de Bordeaux Métropole 
                    </p>
                    <h3>Editeur</h3>
                    <p>
                        <b>Bordeaux Métropole</b><br/>
                        Esplanade Charles-de-Gaulle<br/>
                        33045 Bordeaux Cedex<br/>
                        Tél. : 05 56 99 84 84
                    </p>
                    <p>
                        <b>Directeur de la publication</b><br/>
                        Bertrand DUBOIS<br/>
                        Directeur de cabinet
                    </p>
                    <p>
                        <b>Direction Générale du Numérique et des Systèmes d'information - Administration</b><br/>
                        Cellule Open Data : <a href="https://opendata.bordeaux-metropole.fr/pages/contact/" className="mentions" rel="noreferrer" target="_blank">contact</a>
                    </p>
                    <p>
                        <b>Conception graphique, Développement, Intégration</b><br/>
                        MICROPOLE FRANCE<br/>
                        <a href="https://www.micropole.com/" className="mentions" rel="noreferrer" target="_blank">https://www.micropole.com/</a>
                    </p>
                    <p>
                        <b>Hébergement</b><br/>
                        <a href="https://bordeaux.fr/pgFicheOrga.psml?_nfpb=true&_pageLabel=pgFicheOrga&classofcontent=organisme&id=3211" className="mentions" rel="noreferrer" target="_blank">Aliénor.net</a><br/>
                        SAS inscrite au R.C.S. de Bordeaux sous le numéro B423 856 343<br/>
                        Dont le siège social est situé au BOUSCAT<br/>
                        375 avenue de Tivoli 33110 LE BOUSCAT<br/>
                        Tel : 05.56.69.64.64<br/>
                        <a href="https://www.groupe-aquitem.fr/" className="mentions" rel="noreferrer" target="_blank">https://www.groupe-aquitem.fr/</a>
                    </p>
    
                    <h2>Protection des données à caractère personnel</h2>
                    <p>Dans le cadre de ses missions de service public Bordeaux Métropole met en œuvre différents traitements de données à caractère personnel. A ce titre, elle est soumise aux dispositions du règlement européen 2016/679, Règlement Général pour la Protection des Données, également dénommé « RGPD », entrant en vigueur le 25 mai 2018. Ce texte renforce les obligations relatives à la protection des données et des personnes concernées,  jusque-là  définies par la loi 78-17 « informatique et Libertés ».</p>
    
                    <h3>QUE SONT LES TRAITEMENTS DE DONNÉES À CARACTÈRE PERSONNEL ?</h3>
                    <p>Ce sont tous les traitements manuels ou informatisés relatifs à des données permettant d’identifier directement ou indirectement des personnes physiques (par exemple l’utilisation de données telles que : nom prénom, photographie, e-mail nominatif, numéro d’identification, données de localisation, données propres à l’identité physique, physiologique, économique d’une personne...)</p>
    
                    <h3>DE QUELLE PROTECTION BÉNÉFICIENT LES PERSONNES DONT LES DONNÉES SONT TRAITÉES PAR BORDEAUX MÉTROPOLE ?</h3>
                    <p>Pour chaque traitement, en principe lors de la collecte des données, une information transparente, concise et complète doit être fournie. Cela doit permettre aux personnes concernées d’en comprendre l’objectif et  de les aider à assurer la maîtrise de leurs données en facilitant l’exercice de leurs droits (opposition, accès, rectification, effacement, limitation, portabilité);</p>
    
                    <h3>COORDONNÉES DU DÉLÉGUÉ À LA PROTECTION DES DONNÉES DE BORDEAUX MÉTROPOLE</h3>
                    <p>Bordeaux Métropole a désigné un « Délégué à la protection des données » (acronyme anglais : DPO) qui est l’interlocuteur officiel pour toutes les questions touchant à l’exercice des droits des personnes concernées par un traitement mis en œuvre par un service métropolitain ;</p>
                    <p>Vous pouvez le joindre à l’adresse suivante : <a href="mailto:contact.cnil@bordeaux-metropole.fr" className="mentions"> contact.cnil@bordeaux-metropole.fr</a></p>
                    <p>Pour en savoir plus sur l’exercice de vos droits vous pouvez également consulter le site de la CNIL : <a href="https://www.cnil.fr/" rel="noreferrer" target="_blank">https://www.cnil.fr/</a></p>

                    <h2>Utilisation de notre site Internet</h2>

                    <h3>1. Accéder à notre site Internet</h3>
                    <p>Lorsque vous accédez à notre site Internet, votre navigateur transmet certaines données à notre serveur Web. Cela est nécessaire pour des raisons techniques et pour mettre à votre disposition des informations requises. Pour faciliter l’accès à notre site Internet, les données suivantes sont recueillies, utilisées et conservées pour une durée limitée à 1 an :</p>
                    <ul>
                        <li>Adresse IP</li>
                        <li>Date et heure d’accès</li>
                        <li>Fuseau horaire par rapport à l’heure GMT (Greenwich Mean Time)</li>
                        <li>Statut d’accès/code de statut http</li>
                        <li>Nom de la page accédée</li>
                        <li>Navigateur, paramètres de langue, version du logiciel navigateur et interface</li>
                    </ul>
                    <p>Nous conserverons ces données pour une durée limitée dans le temps afin de pouvoir instaurer un suivi des données personnelles en cas d’accès ou de tentative d’accès non autorisé à nos serveurs.</p>
                    <h3>2. Utilisation des formulaires de Contact</h3>
                    <p>Ce Site Internet contient <a href="https://opendata.bordeaux-metropole.fr/pages/contact/" className="mentions" rel="noreferrer" target="_blank">un formulaire de contact </a>qui vous permettra de nous contacter directement. Dans ce cas, vous serez amené(e) à nous fournir les informations suivantes :</p>
                    <ul>
                        <li>identité</li>
                        <li>adresse mail</li>
                    </ul>
                    <p>Nous collecterons, traiterons et utiliserons les informations que vous nous aurez fournies par ce formulaire de contact exclusivement pour traiter votre demande.</p>

                    <h2>Information concernant vos droits</h2>
                    <p>Conformément à la réglementation applicable en matière de protection des données personnelles et aux dispositions du Règlement Général sur la Protection des Données (RGPD) UE/2016/679 du 27 avril 2016, vous disposez des droits suivants sur les données qui vous concernent :</p>
                    <ul>
                        <li>Droit d'information sur vos données personnelles que nous stockons ;</li>
                        <li>Droit de demander la correction, la suppression ou la limitation du traitement de vos données personnelles ;</li>
                        <li>Droit de vous opposer à un traitement pour des raisons d'intérêt légitime, d'intérêt public ou de profilage, à moins que nous puissions démontrer que des motifs légitimes et impérieux prévalent sur vos intérêts, droits et libertés, ou qu'un tel traitement est effectué aux fins de revendication, de l'exercice ou de la défense de droits en justice ;</li>
                        <li>Droit à la portabilité de vos données ;</li>
                    </ul>
                    <p>Vous pouvez à tout moment révoquer votre consentement à la collecte, au traitement et à l'utilisation future de vos données personnelles. Pour plus d’informations, veuillez consulter les chapitres ci-dessus décrivant les traitements de données basés sur votre consentement. Pour exercer ces droits ou pour toute question sur le traitement de vos données, vous pouvez Bordeaux Métropole par courriel : contact.cnil@bordeaux-metropole.fr ou par courrier à l’adresse suivante :</p>
                    <p>
                        <b>Bordeaux Métropole</b><br/>
                        Direction des Affaires Juridiques<br/>
                        Cellule protection des données<br/>
                        Esplanade Charles-de-Gaulle<br/>
                        33045 Bordeaux Cedex
                    </p>
    
                    <p>Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés, vous pouvez adresser une réclamation à la Commission Nationale Informatique et Libertés (CNIL), autorité compétente en matière de protection des données personnelles.</p>

                    <h2>Modification de la politique de confidentialité</h2>
                    <p>Nous pouvons parfois actualiser notre politique de confidentialité. Ces mises à jour seront publiées sur notre site Internet. Toute modification entrera en vigueur dès sa publication. Nous vous recommandons donc de visiter régulièrement notre site Internet pour vous tenir au courant.</p>

                    <h2>Droit de reproduction</h2>
                    <p>La reproduction des pages de ce site est possible à condition de respecter l'intégrité des documents reproduits (pas de modification ni altération d'aucune sorte) et de bien vouloir mentionner la source. Toutes les données et applications ont leur propre licence. Les logos de Bordeaux Métropole et des communes qu’elle regroupe en Accès Libre sont la propriété de ces collectivités. Ainsi, toute reproduction ou modification (totale ou partielle) de ces éléments graphiques sans autorisation préalable et écrite est interdite. Pour tous renseignements, vous pouvez nous contacter via le formulaire de contact ou par courrier postal.</p>

                    <h2>Les liens vers le site datalab.bordeaux-metrople.fr</h2>
                    <p>Bordeaux Métropole autorise tout site Internet ou tout support à mettre en place un lien hypertexte en direction de son contenu à l'exception de ceux diffusant des contenus à caractère polémique, pornographique, xénophobe, contraire à la décence ou aux bonnes mœurs. Pour autant, l'intégralité de la page liée doit être respectée, et aucune imbrication dans un autre site ne doit être réalisée. De même, le lien doit indiquer clairement la nature des contenus, ainsi que l'adresse exacte de la page. Toute utilisation de liens vers le site data.rennesmetropole.fr à des fins commerciales et publicitaires est interdite.</p>

                    <h2>Politique de modération des commentaires</h2>

                    <p>S’agissant des échanges sur le forum dédié, conformément aux lois en vigueur seront systématiquement supprimés :</p>
                    <ul>
                        <li>l’incitation à la haine raciale et les propos racistes, antisémites et xénophobes, la négation de crimes contre l’Humanité et des génocides reconnus, et l’apologie des crimes de guerre et/ou du terrorisme</li>
                        <li>les propos à caractère homophobe ou sexiste, les propos d’une nature violente, pornographique ou pédophile</li>
                        <li>la diffamation et les injures entre internautes ou à l’égard d’une tierce personne- les atteintes à la vie privée ou à la présomption d’innocence, l’usurpation d’identité</li>
                        <li>le non-respect du droit d’auteur, de la propriété intellectuelle et de licence</li>
                        <li>l’incitation à la commission de crimes ou de délits, l’apologie des stupéfiants</li>
                        <li>l’appel au meurtre et l’incitation au suicide, la promotion d’une organisation reconnue comme sectaire.</li>
                    </ul>

                    <h2>Responsabilités et garanties pour l'utilisation du site</h2>

                    <p>
                        Bordeaux Métropole ne garantit pas que le site sera exempt d'anomalies, d'erreurs ou de bugs, ni que celles-ci pourront être corrigées, ni que le site fonctionnera sans interruption ou pannes, ni encore qu'il soit compatible avec un matériel ou une configuration particulière autre que celle expressément validée par Bordeaux Métropole.<br/>
                        Bordeaux Métropole ne pourra en aucun cas être tenu pour responsable de dysfonctionnements imputables à des logiciels de tiers que ceux-ci soient ou non incorporés dans le site ou mise en œuvre à partir de celui-ci.<br/>
                        En aucun cas, Bordeaux Métropole ne sera responsable de tout type de dommage prévisible ou imprévisible (incluant la perte de profits ou d'opportunité) découlant de l'utilisation ou de l'impossibilité totale ou partielle d'utiliser le site. Enfin, Bordeaux Métropole ne pouvant contrôler l'ensemble des sites sur lesquels elle renvoie par l'intermédiaire de liens hypertextes, qui n'existent que pour faciliter les recherches de l'utilisateur, n'est aucun cas responsable de leur contenu.<br/>
                        Bordeaux Métropole s'efforce de contrôler au mieux la pertinence et la véracité des contenus du site Internet. Si toutefois, malgré nos efforts, vous constatiez un problème, nous vous serions reconnaissants de bien vouloir nous en informer, par le biais du formulaire de contact prévu à cet effet.
                    </p>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default MentionsLegales
