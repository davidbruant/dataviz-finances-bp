import React from 'react'
import { HashRouter, Routes, Route } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import { setPhaseBudgetaire } from './redux/datavizSlice'
import VizFinances from './containers/VizFinances'
import ExplicationCompteAdministratif from './containers/ExplicationCompteAdministratif'

export default () => {    
    const dispatch = useDispatch()

    dispatch(setPhaseBudgetaire("CA"))
    
    return (
        <HashRouter>
            <Routes>
                <Route exact path="/" element={<VizFinances présentation="Agregation" />} />
                <Route exact path="fonction" element={ <VizFinances présentation="Fonction" /> } /> 
                <Route exact path="compte-administratif" element={ <ExplicationCompteAdministratif /> } />
            </Routes>
        </HashRouter>
    )
}
