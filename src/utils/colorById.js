
const predefinedColors = [
    "rgb(98, 33, 129)", "rgb(226, 0, 122)", "rgb(32, 86, 164)", "rgb(26, 26, 132)", 
    "rgb(253, 202, 0)", "rgb(26, 23, 27)", "rgb(29, 140, 161)", "rgb(0, 158, 224)", 
    "rgb(242, 148, 0)", "rgb(151, 191, 13)",

    "rgb(99, 33, 129)", "rgb(227, 0, 122)", "rgb(33, 86, 164)", "rgb(27, 26, 132)", 
    "rgb(254, 202, 0)", "rgb(27, 23, 27)", "rgb(28, 140, 161)", "rgb(0, 159, 224)", 
];

const colorById = new Map()

export default (id, excludeSet) => {      
    if (colorById.has(id)) {
        return colorById.get(id)
    } 
    else {
        const applicableColor = predefinedColors.find(c => !excludeSet || !excludeSet.has(c))
        if (!applicableColor) {
            throw new Error(`Pas trouvé de couleur pertinente. Peut-être que le choix est trop restreint`)
        }
        
        colorById.set(id, applicableColor)

        return applicableColor
    }
}