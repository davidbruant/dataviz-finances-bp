import { flatten } from './common.js'

/**
 * This function returns the ancestry of a given element within a tree 
 * in order of closest to furthest
 */

const findAncestry = (element, fullTree) => {
    let ancestry = []
    let childInSearchOfParents = element

    const allNodes = flatten(fullTree)
    while(childInSearchOfParents) {
        const id = childInSearchOfParents.id
        const foundParent = allNodes.find(
            n => (n.children ? n.children.find(c => c.id === id) : undefined)
        )
        
        if(foundParent){ 
            ancestry.push(foundParent)
            childInSearchOfParents = foundParent
        }
        else
            childInSearchOfParents = undefined
    }

    return ancestry
}

export default findAncestry