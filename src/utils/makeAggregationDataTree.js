//@ts-check

import "../types.d.js"
import { buildTree } from "./common.js";

/**
 * @param {FinanceData} financedata
 * @param {any} labelsDocBudg
 * @returns {VisualisedDataTree} 
 */
export default (financedata, labelsDocBudg) => {
    /** @type { VisualisedDataTree } */
    const aggregationDataTree = {}

    financedata.aggregations.forEach((aggregationYear) => {
        /** @type {AugmentedAggregationNode} */
        // @ts-ignore
        let data = buildTree(aggregationYear.aggregation, labelsDocBudg)

        aggregationDataTree[aggregationYear.year] = {
            D: data.children.find(c => c.id === "D"),
            R: data.children.find(c => c.id === "R")
        }
    })

    return aggregationDataTree
}