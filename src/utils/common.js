//@ts-check

import '../types.d.js';

import {isAugmentedAggregationNode} from '../predicates.js'

import colorById from './colorById.js';


export const getApplicationTitle = (labels, key) => {
    return `${labels['application.name']} - ${labels[key]}`
}


/**
 * @param {AugmentedAggregationTree} current 
 * @param {any} labelsDocBudg 
 * @param {string} parentId 
 * @returns {AugmentedAggregationTree}
 */
export const buildTree = (current, labelsDocBudg, parentId="") => {
    current.deepId = parentId === '' ? current.id : parentId + '.' + current.id

    // les couleurs sont majoritairement attribuées aux enfants depuis le parent
    // cette ligne résoud le cas racine
    if(!current.color){
        current.color = colorById(current.id)
    }

    if (isAugmentedAggregationNode(current)) {
        let somme = 0
        const usedColors = new Set([current.color])

        for (const child of current.children) {
            // apply color before recursion
            child.color = colorById(child.id, usedColors)
            usedColors.add(child.color)

            let newChild = buildTree(child, labelsDocBudg, current.deepId)
        
            somme += newChild.cumulatedSize
        }
        
        current.cumulatedSize = somme
        
        current.children.sort(({cumulatedSize: cS1}, {cumulatedSize: cS2}) => cS2-cS1)

    } else if (current.elements) {
        let somme = 0
        let newChildren = []

        for(let i=0; i<current.elements.length; i++) {
            const currentElement = current.elements[i]
            somme += currentElement.Montant
            currentElement.idNat = currentElement.Nature
            currentElement.idFonct = currentElement.Fonction

            if(!labelsDocBudg.natureLabels[currentElement.Nature]){
                console.log('currentElement', JSON.parse(JSON.stringify(currentElement)))
                throw new Error(`Label manquant pour la nature ${currentElement.Nature}`)
            }

            if(!labelsDocBudg.fonctionLabels[currentElement.Fonction]){
                throw new Error(`Label manquant pour la fonction ${currentElement.Fonction}`)
            }

            currentElement.name = labelsDocBudg.natureLabels[currentElement.Nature]
            currentElement.nameFonction = labelsDocBudg.fonctionLabels[currentElement.Fonction]
            currentElement.size = currentElement.Montant
            currentElement.cumulatedSize = currentElement.Montant
            delete currentElement.Nature
            delete currentElement.Fonction
            delete currentElement.Montant
            delete currentElement.CodRD
            newChildren.push(currentElement)
        }
        current.cumulatedSize = somme
    }
    return current
}

/*const sortTree = (current) => {
    if (current.children) {
        current.children.sort( (a, b) => a.cumulatedSize === b.cumulatedSize ? 0 : a.cumulatedSize > b.cumulatedSize ? 1 : -1 )
        for(let i=0; i<current.elements.length; i++) {
            current.children[i] = sortTree(current.children[i])
        }
    }

    return current
}*/

function visit(node, f) {
    f(node)
    
    if(node.children) {
        const children = Array.isArray(node.children) ?
            node.children :
            Array.from(node.children.values())

        children.forEach(child => {
            visit(child, f)
        })
    }
}

export function flatten(node) {
    const result = []
    visit(node, n => result.push(n))

    return result
}

/**
 * 
 * @param {any} propertyValue 
 * @param {string} propertyName 
 * @param {AugmentedAggregationTree} dataTree 
 * @returns {AugmentedAggregationTree | null}
 */
const findInTree = (propertyValue, propertyName, dataTree) => {
    if (dataTree[propertyName] === propertyValue) {
        return dataTree
    } else {
        if (isAugmentedAggregationNode(dataTree)) {
            for(let i=0; i<dataTree.children.length; i++) {
                let res = findInTree(propertyValue, propertyName, dataTree.children[i])
                if (res) {
                    return res
                }
            }
        }
        return null
    }
}

/**
 * 
 * @param {any} deepId 
 * @param {AugmentedAggregationTree} dataTree 
 * @returns {ReturnType<findInTree>}
 */
export const findInTreeByDeepId = (deepId, dataTree) => {
    return findInTree(deepId, 'deepId', dataTree)
}
