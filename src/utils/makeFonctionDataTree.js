//@ts-check

import "../types.d.js"

import {buildTree as buildAugmentedTree} from "./common.js"

/**
 * 
 * @param {LigneDocumentBudgetaire[]} rows
 * @param {any} labels
 * @returns {AggregationTree[]}
 */
function buildFonctionTree(rows, labels){

    /** @type { Map<string, AggregationLeaf> } */
    let rowsByFunction = new Map();

    for(const row of rows){
        const f = row["Fonction"];
        
        if(!rowsByFunction.has(f)){
            /** @type {AggregationTree & {Fonction: string}} */
            const group = {
                id: `F-${f}`,
                name: labels.fonctionLabels[f],
                Fonction: f,
                elements: []
            }
            
            rowsByFunction.set(f, group)
        }

        /** @type { NonNullable<ReturnType<typeof rowsByFunction.get>> } */
        // @ts-ignore
        const group = rowsByFunction.get(f)        
        group.elements.push(row)
    }

    /** @type { Set<NonNullable<ReturnType<typeof rowsByFunction.get>> | AggregationTree> } */
    let movingSet = new Set([...rowsByFunction.values()]);

    const maxFonctionSize = Math.max(...[...rowsByFunction.keys()].map(f => f.length))

    let currentlyRegroupedFonctionSize = maxFonctionSize;

    while(currentlyRegroupedFonctionSize >= 2){
    
        const rowsOfCurrentFonctionSize = [...movingSet]
            // eslint-disable-next-line
            .filter(r => r["Fonction"].length === currentlyRegroupedFonctionSize) 

        const parentsOfThisSize = new Map();

        for(const row of rowsOfCurrentFonctionSize){
            // remove last digit
            const parentFonction = row["Fonction"].slice(0, currentlyRegroupedFonctionSize - 1)

            if(!parentsOfThisSize.has(parentFonction)){
                /** @type {AggregationTree & {Fonction: string}} */
                const parent = {
                    id: `F-${parentFonction}`,
                    name: labels.fonctionLabels[parentFonction],
                    Fonction: parentFonction,
                    children: []
                }
                
                parentsOfThisSize.set(parentFonction, parent)

                movingSet.add(parent)
            }

            const parent = parentsOfThisSize.get(parentFonction)
            parent.children.push(row)

            movingSet.delete(row)
        }

        currentlyRegroupedFonctionSize = currentlyRegroupedFonctionSize - 1
    }


    // @ts-ignore
    return [...movingSet]
}

/**
 * @param {FinanceData} financedata
 * @param {any} labels
 * @returns {VisualisedDataTree} 
 */
export default (financedata, labels) => {
    /** @type { VisualisedDataTree } */
    const financeDataTree = {}

    financedata.documentBudgetaires.forEach(({Exer, rows}) => {

        const Rrows = rows.filter(r => r.CodRD === "R")
        const Drows = rows.filter(r => r.CodRD === "D")

        /** @type {AggregationTree} */
        const RDataTree = {
            id: "R",
            name: "Recettes",
            children: buildFonctionTree(Rrows, labels)
        }

        /** @type {AggregationTree} */
        const DDataTree = {
            id: "D",
            name: "Dépenses",
            children: buildFonctionTree(Drows, labels)
        }

        financeDataTree[Exer] = {
            // @ts-ignore
            R: buildAugmentedTree(RDataTree, labels),
            // @ts-ignore
            D: buildAugmentedTree(DDataTree, labels),
        }

    })

    return financeDataTree
}