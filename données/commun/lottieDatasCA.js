export default [
    {
        id: "#firstLottie",
        actions: [
            {
               visibility: [0.0,0.15],
               type: 'stop', 
               frames: [0],
            },	
            {
                visibility: [0.15,1.00],
                type: 'play', 
            },
            {
                visibility: [0.4, 1.00],
                type: 'stop', 
                frames: [400],
            }	
       ]
    },
    {
        id: "#secondLottie",
        actions: [
            {
                visibility: [0.0, 0.1],
                type: 'stop',
                frames: [0],
            }, 	
            {
                visibility: [0.1, 1.00],
                type: 'play', 
            },
                 {
                visibility: [0.7, 1.00],
                type: 'stop',
                 frames: [400],
            }, 
        ]
    },
    {
        id: "#threeLottie",
        actions: [
            {
                visibility: [0.0, 0.4],
                type: 'stop',
                frames: [0],
            }, 
            {
                visibility: [0.4, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.8, 1.00],
                type: 'stop',
                frames: [400],
            }
        ]
    },
    {
        id: "#fourLottie",
        actions: [
            {
                visibility: [0.0, 0.4],
                type: 'stop',
                frames: [0],
            }, 
                
            {
                visibility: [0.4, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.99, 1.00],
                type: 'stop',
                frames: [300],
            }
        ]
    },
    /*{
        id: "#balanceLottie",
        actions: [
            {
                visibility: [0.0, 0.3],
                type: 'stop',
                frames: [0],
            }, 
                
            {
                visibility: [0.3, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.7, 1.00],
                type: 'stop',
                frames: [600],
            } 
        ]
    },*/
    /*{
        id: "#bgLottiePrice1",
        actions: [
            {
                visibility: [0.0, 0.1],
                type: 'stop',
                frames: [0],
            }, 	
            {
                visibility: [0.1, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.6, 1.00],
                type: 'stop',
                frames: [130],
            }
        ]
    },
    {
        id: "#sixLottie-mob",
        actions: [
            {
                visibility: [0.0, 0.4],
                type: 'stop',
                frames: [0],
            }, 
            {
                visibility: [0.4, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.4, 1.00],
                type: 'stop',
                frames: [220],
            }
        ]
    },
    {
        id: "#sixLottie",
        actions: [
            {
                visibility: [0.0, 0.2],
                type: 'stop',
                frames: [0],
            }, 
                
            {
                visibility: [0.2, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.6, 1.00],
                type: 'stop',
                frames: [220],
            }
        ]
    },*/
    /*{
        id: "#bgLottiePrice2",
        actions: [
            {
                visibility: [0.0, 0.5],
                type: 'stop',
                frames: [0],
            }, 
                
            {
                visibility: [0.5, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.7, 1.00],
                type: 'stop',
                frames: [220],
            }
        ]
    },*/
    /*{ // flèche qui descend
        id: "#sevenLottie",
        actions: [
            {
                visibility: [0.0, 0.4],
                type: 'stop',
                frames: [0],
            }, 
                
            {
                visibility: [0.4, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.6, 1.00],
                type: 'stop',
                frames: [220],
            }
        ]
    },*/
    /*{
        id: "#balanceLottieTwo",
        actions: [
            {
                visibility: [0.0, 0.4],
                type: 'stop',
                 frames: [0],
            }, 
                
            {
                visibility: [0.4, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.6, 1.00],
                type: 'stop',
                frames: [220],
            }
        ]
    },*/
    /*{
        id: "#bgLottiePrice3",
        actions: [
            {
                visibility: [0.0, 0.3],
                type: 'stop',
                frames: [0],
            }, 
                
            {
                visibility: [0.3, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.5, 1.00],
                type: 'stop',
                frames: [120],
            }
        ]
    },*/
    /*{
        id: "#nineLottie",
        actions: [
            {
                visibility: [0.0, 0.3],
                type: 'stop',
                 frames: [0],
            }, 
            {
                visibility: [0.3, 1.00],
                type: 'play', 
            },
            {
                visibility: [0.5, 1.00],
                type: 'stop',
                frames: [120],
            }
        ]
    },
    {
        id: "#eightLottie",
        actions: [
            {
                visibility:[0.0, 0.3],
                type: "seek",
                frames: [0, 64]
            },
            {
                visibility:[0.3 , 0.32],
                type: "play",
                frames: [0,64]
            }
        ]
    },*/
    /*{
        id: "#bgLottiePrice4",
        actions: [
            {
                visibility:[0.1 , 0.5],
                type: "seek",
                frames: [0,85]
            }
        ]
    },
    {
        id: "#twelve2Lottie",
        actions: [
            {
                visibility:[0.1 , 0.5],
                type: "seek",
                frames: [0,85]
            }
        ]
    },*/
    {
        id: "#tenLottie",
        actions: [
            {
                visibility:[0.1 , 0.5],
                type: "seek",
                frames: [0,85]
            }
        ]
    },
    {
        id: "#elevenLottie",
        actions: [
            {
                visibility:[0.1 , 0.5],
                type: "seek",
                frames: [0,85]
            }
        ]
    },
    /*{
        id: "#twelveLottie",
        actions: [
            {
                visibility:[0.0, 0.1],
                type: "stop",
                frames : [0],   
            }, 
            {
                visibility:[0.3, 1.00],
                type: "play",
            },
            {
                visibility:[0.9, 1.00],
                type: "stop",
                frames : [250],
            }
        ]
    },*/
    /*{
        id: "#balanceLottieThree",
        actions: [
            {
                visibility:[0.0, 0.3],
                type: "stop",
                frames: [0],
            },
            {
                visibility:[0.3, 1.00],
                type: "play",
            },
            {
                visibility:[0.99, 1.00],
                type: "stop",
                frames: [240],
            },
        ]
    },*/
    /*{
        id: "#bgLottieBtn",
        actions: [
            {
                visibility:[0.0, 0.05],
                type: "stop",
                frames: [0],
            }, 
            {
                visibility:[0.3, 1.00],
                type: "play",
            },
            {
                visibility:[0.6, 1.00],
                type: "stop",
                frames: [300],
            }
        ]
    },*/
]
