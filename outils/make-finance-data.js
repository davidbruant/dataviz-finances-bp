import {join, resolve} from 'path'

import getPlansDeCompte from './finances/get-plans-de-compte.js'
import {fromXMLDocument} from './finances/plan-de-compte.js'
import buildFinanceData from './finances/build-finance-data-from-dir.js'

const BUILD_FINANCE_BORDEAUX_DIR = 'public/bordeaux/finances'
const SOURCE_FINANCE_DIR_BORDEAUX_BP = 'données/bordeaux/finances/BP'
const SOURCE_FINANCE_DIR_BORDEAUX_CA = 'données/bordeaux/finances/CA'
const SOURCE_PLAN_DE_COMPTE_DIR = 'données/plans-de-compte'

const plansDeComptesP = getPlansDeCompte(SOURCE_PLAN_DE_COMPTE_DIR)
.then(pdcs => pdcs.map(fromXMLDocument))

plansDeComptesP.then(plansDeCompte => {
    return Promise.all([
        buildFinanceData(
            resolve(SOURCE_FINANCE_DIR_BORDEAUX_BP), 
            join(BUILD_FINANCE_BORDEAUX_DIR, 'finance-data-bp.json'),
            plansDeCompte
        ),
        buildFinanceData(
            resolve(SOURCE_FINANCE_DIR_BORDEAUX_CA), 
            join(BUILD_FINANCE_BORDEAUX_DIR, 'finance-data-ca.json'),
            plansDeCompte
        )
    ])
})
