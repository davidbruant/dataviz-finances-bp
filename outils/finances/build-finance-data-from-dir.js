//@ts-check

import {join} from 'path'
import {readFile, writeFile, readdir} from 'node:fs/promises'

import {DOMParser} from '@xmldom/xmldom'

import xmlDocumentToDocumentBudgetaire from './xml-to-document-budgetaire.js'
import makeAggregateFunction from './make-aggregate-function.js'

const aggregationDescription = JSON.parse(
    await readFile('données/commun/description-agrégation.json', 'utf-8')
)

/**
 * @param {string} sourceDirectory dossier (chemin absolu) avec des fichiers <DocumentBudgetaire> à compiler
 * @param {string} outFile fichier de sortie (chemin absolu)
 * @returns {Promise<void>}
 */
export default function(sourceDirectory, outFile, plansDeCompte){

    return readdir(sourceDirectory)
        .then(files => {
            return Promise.all(files.map(f => {
                return readFile(join(sourceDirectory, f), {encoding: 'utf-8'})
                .then( str => (new DOMParser()).parseFromString(str, "text/xml") )
                .then(xmlDocumentToDocumentBudgetaire)
            }))
        })
        .then( documentBudgetaires => {
            return {
                documentBudgetaires : documentBudgetaires.map(db => {
                    return Object.assign({}, db, {rows: [...db.rows]})
                }),
                aggregations: documentBudgetaires.map(docBudg => {
                    const year = docBudg['Exer']
                    const planDeCompte = plansDeCompte.find(pdc => pdc.Exer === year)

                    if(!planDeCompte){
                        throw new Error(`Plan de compte manquant pour l'année ${year}`)
                    }

                    return {
                        year,
                        aggregation: makeAggregateFunction(aggregationDescription, planDeCompte)(docBudg)
                    }
                })
            }
        })
        .then(data => {
            return writeFile(outFile, JSON.stringify(data, undefined, 2), 'utf-8')
        })
}