import {sum} from 'd3-array'

import {makeLigneBudgetId} from './doc-budg-data-structure.js'

export default function xmlToDocumentBudgetaire(doc){
    const BlocBudget = doc.getElementsByTagName('BlocBudget')[0]

    const Exer = Number(BlocBudget.getElementsByTagName('Exer')[0].getAttribute('V'))
    
    // Explication de NatDec : https://github.com/DavidBruant/colors-of-the-finances/blob/master/docs/format-fichier.md#en-t%C3%AAte-budget
    const NatDec = BlocBudget.getElementsByTagName('NatDec')[0].getAttribute('V')

    console.log("xmlToDocumentBudgetaire", Exer, NatDec)


    const lignes = Array.from(doc.getElementsByTagName('LigneBudget'))
    .map(l => {
        const ret = {
            'Nature': l.getElementsByTagName('Nature')[0].getAttribute('V'),
            'Fonction': l.getElementsByTagName('Fonction')[0].getAttribute('V'),
            'CodRD': l.getElementsByTagName('CodRD')[0].getAttribute('V'),
            'MtPrev': Number(l.getElementsByTagName('MtPrev')[0].getAttribute('V')),
            'MtReal': Number(l.getElementsByTagName('MtReal')[0].getAttribute('V')),
            'OpBudg': l.getElementsByTagName('OpBudg')[0].getAttribute('V')
        }

        return ret
    })
    .filter(({Nature, Fonction, OpBudg, MtPrev, MtReal}) => {
        // Enlever les opérations d'ordre
        const isOR = OpBudg === '0'
        // Enlever les lignes budgétaires où tous les montants sont à 0
        // NatDec === "10" est une nouveauté du CFU
        const has0Amount = (NatDec !== "09" && MtPrev !== 0) || ((NatDec === "09" || NatDec === "10") && MtReal !== 0);

        return isOR && has0Amount &&
            !(Nature === '001' && Fonction === '01') &&
            !(Nature === '002' && Fonction === '0202')
    })

    
    // Dans les fichiers <DocumentBudgetaire>, le même triplet (CodRD, Nature, Fonction)
    // peut apparaître plusieurs fois. On les groupe en un seul élément
    const xmlRowsById = new Map()

    for(const r of lignes){
        const id = makeLigneBudgetId(r)

        const idRows = xmlRowsById.get(id) || []
        idRows.push(r)
        xmlRowsById.set(id, idRows)
    }

    return {
        LibelleColl: doc.getElementsByTagName('LibelleColl')[0].getAttribute('V'),
        Nomenclature: doc.getElementsByTagName('Nomenclature')[0].getAttribute('V'),
        NatDec,
        Exer,
        IdColl: doc.getElementsByTagName('IdColl')[0].getAttribute('V'),

        rows: new Set([...xmlRowsById.values()]
            .map(xmlRows => {
                const amount = sum(xmlRows.map(
                    // Pour les comptes administratifs, prendre MtReal, sinon prendre MtPrev
                    r => (NatDec === "09" ? r.MtReal : r.MtPrev)
                ))
                
                const r = xmlRows[0]

                return Object.assign(
                    {},
                    r,
                    { 'Montant': amount }
                )
            })
        )
    }

}
