//@ts-check
import {readFile, writeFile} from 'node:fs/promises'

import {dsvFormat} from 'd3-dsv'

const COMMUN_PATH = 'données/commun/textes/commun.csv'
const BP_PATH = 'données/commun/textes/bp.csv'
const CA_PATH = 'données/commun/textes/ca.csv'

const {parse: parseLabels, format: stringifyLabels} = dsvFormat(';')

const commun = readFile(COMMUN_PATH, 'utf-8').then(s => parseLabels(s))
const bp = readFile(BP_PATH, 'utf-8').then(s => parseLabels(s))
const ca = readFile(CA_PATH, 'utf-8').then(s => parseLabels(s))

// BP
Promise.all([commun, bp])
.then(([commun, bp]) => {
    // @ts-ignore
    const content = [].concat(commun, bp)
    return writeFile('public/data/labels-bp.csv', stringifyLabels(content), 'utf-8')
})


// CA
Promise.all([commun, ca])
.then(([commun, ca]) => {
    // @ts-ignore
    const content = [].concat(commun, ca)
    return writeFile('public/data/labels-ca.csv', stringifyLabels(content), 'utf-8')
})
