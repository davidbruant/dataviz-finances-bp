import nodeResolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import babel from '@rollup/plugin-babel';
import replace from '@rollup/plugin-replace';
import json from '@rollup/plugin-json';
import terser from '@rollup/plugin-terser';

import _package from './package.json' with { type: "json" };

const NODE_ENV = process.env.NODE_ENV || "development"

const plugins = [
    nodeResolve({
        extensions: ['.js', '.jsx']
    }),
    babel({
        babelHelpers: 'runtime',
        presets: ['@babel/preset-react'],
        extensions: ['.js', '.jsx'],
        plugins: ["@babel/plugin-transform-runtime"]
    }),
    commonjs(),
    replace({
        preventAssignment: false,
        'process.env.NODE_ENV': `"${NODE_ENV}"`,
        'process.env.APP_VERSION': `"${_package.version}"`,
        'process.env.BASE_URL': `"${NODE_ENV === "development" ? '/' : '/dataviz-bp/bordeaux/'}"`
    }),
    json(),
    NODE_ENV === 'production' ? terser() : undefined
]

export default [
    {
        input: 'src/bp.js',
        output: {
            file: 'public/rollup-bundle-bp.js',
            format: 'iife',
            assetFileNames: '[name][extname]'
        },
        plugins
    },
    {
        input: 'src/ca.js',
        output: {
            file: 'public/rollup-bundle-ca.js',
            format: 'iife',
            assetFileNames: '[name][extname]'
        },
        plugins
    }
]